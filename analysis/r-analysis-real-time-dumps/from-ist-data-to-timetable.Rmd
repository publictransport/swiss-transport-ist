---
title: "From IST data to time table"
output: pdf_document
params:
  trip.ist.in.filename: data/cff-delays-lausanne-geneve.tsv

  trip.ist.clean.out.filename: data/cff-ist-delays-lausanne-geneve-clean.tsv
  timetable.out.filename: data/dt-timetable.tsv
  metadata.out.filename: data/metadata.json
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## R Markdown

```{r init, echo=FALSE, warning=FALSE, message=FALSE}
library(lubridate)
library(dplyr)
library(rjson)
```

The purpose of this workbook is to transform the raw `ist` data from `r params$trip.ist.in.filename` (filtered out between two given stations by a [go program](../../components/ist-ingestor/app/allTrainBetween.go)) and to build a clean and aggregated time table data frame into `r params$timetable.out.filename`.

Extra metat data will be saved into `r params$metadata.out.filename`

# Data acquisition
Parameters are extracted from the knitr ones

```{r parse-params}
file.tsv=params$trip.ist.in.filename
timetable.out.filename = params$timetable.out.filename
trip.ist.clean.out.filename = params$trip.ist.clean.out.filename
metadata.out.filename = params$metadata.out.filename
```

Load original file
```{r data-load}
dt.orig = read.delim(file.tsv)
```

Remove duplicate (for example, 2018 January 1st was populated with duplicate information)

```{r data-cleaning-duplicate, echo=FALSE}
dt = distinct(dt.orig)
```


Date and boolean value are converted out from string fields.
```{r data-cleaning-date-conversion}
dt$ts_arrival_actual = ymd_hms(dt$ts_arrival_actual)
dt$ts_arrival_time = ymd_hms(dt$ts_arrival_time)
dt$ts_departure_actual = ymd_hms(dt$ts_departure_actual)
dt$ts_departure_time = ymd_hms(dt$ts_departure_time)
dt$day=dmy(dt$day)
dt$day.trip.id=as.factor(paste(format(dt$day, "%Y%m%d"),dt$trip_id))

dt$is_cancelled = dt$is_cancelled=='true'
dt$is_additional_driving = dt$is_additional_driving=='true'
```

Time information is enriched: weekday, hourly timestamps, delta schedule/atual measured in minutes.

```{r data-cleaning-date-enrichment}
dt$day.type = as.factor(
  ifelse(
    weekdays(dt$day) %in% c('Sunday', 'Saturday'),
    'weekends',
    'week days'
  )
)

dt$delta.arrival.min=as.numeric(
  difftime(dt$ts_arrival_actual,
           dt$ts_arrival_time,
           unit="mins"
  )
)
dt$delta.departure.min=as.numeric(
  difftime(
    dt$ts_departure_actual,
    dt$ts_departure_time,
    unit="mins"
  )
)

dt$h.departure.time = difftime(dt$ts_departure_time, dt$day)
dt$h.departure.actual = difftime(dt$ts_departure_actual, dt$day)
dt$h.arrival.time = difftime(dt$ts_arrival_time, dt$day)
dt$h.arrival.actual = difftime(dt$ts_arrival_actual, dt$day)

```

Overall time period is extracted
```{r data-cleaning-date-period}
period.date.from=format(min(dt$day), format="%m/%d/%Y")
period.date.to=format(max(dt$day), format="%m/%d/%Y")
period.nb.days= as.numeric(difftime(max(dt$day), min(dt$day),unit='days'))
```

Observations are sorted by time and trip_id (as it is not alwys the case in the input file).
```{r data-prep-sort}
dt = transform(
  dt, 
  ts.avg = pmin(ts_arrival_time, ts_departure_time, na.rm = TRUE)
)

dt = dt %>%
  arrange(ts.avg) %>%
  arrange(trip_id)
```



## Add colomuns with the next stop

```{r add-next}

dt_next = function(field){
  .x=dt[[field]][2:nrow(dt)]
  .x[nrow(dt)]= NA
  .x
}
msk.next.same.trip.day = dt_next('trip_id')==dt$trip_id & dt_next('day') == dt$day

for (f in c(
  'station_name',
  'trip_distance',
  'h.arrival.time',
  'h.arrival.actual', 
  'delta.arrival.min',
  'h.departure.time',
  'h.departure.time',
  'delta.departure.min'
)
){
  .newf=paste('next',f,sep='.')
  dt[[.newf]]=dt_next(f)
  dt[[.newf]][!msk.next.same.trip.day]=NA
}

#remove arrival information for head stops in the segment
#we are not interested by what happens before
msk.prev.not.same.trip.day=c(TRUE, !msk.next.same.trip.day[1:nrow(dt)-1])

for (f in c(
  'h.arrival.time',
  'h.arrival.actual', 
  'delta.arrival.min'
)
){
  dt[[f]] = ifelse(msk.prev.not.same.trip.day, NA, dt[[f]])
}

dt$segment.tag=paste(dt$station_name,
                     dt$h.departure.time,
                     dt$next.station_name,
                     dt$next.h.arrival.time
)

```

### Frequency 

#### compute segment frequency

Add a segment frequency, based on stop pairs and type of day (weekends vs weeks days)

```{r actual-trip-compute-frequency}

dt.day.trip.tag = dt %>%
  group_by(day, day.trip.id) %>%
  summarise(
    station.names = paste0(station_name, collapse="/"),
    departure.times = paste0(h.arrival.time, collapse="/")
  )

dt.day.trip.tag$signature = as.factor(
  paste(
    dt.day.trip.tag$station.names,
    dt.day.trip.tag$departure.times
  )
)

dt.segment.freq = dt %>%
  group_by(segment.tag, day.type) %>%
  summarise (frequency=n()/period.nb.days) 

# count week/weekend days
dt.day.type.count = dt %>%
  select(day, day.type) %>%
  distinct() %>%
  group_by(day.type) %>%
  summarise(count=n())

dt.segment.freq = merge(dt.segment.freq, dt.day.type.count)
dt.segment.freq$frequency = dt.segment.freq$frequency / dt.segment.freq$count * period.nb.days
dt.segment.freq$count =  NULL

dt$frequency = NULL
dt = merge(dt, dt.segment.freq)
```

Sort trips by `trip_id` and average (departure/arrival) timestamps.

```{r actual-trip-sort}
dt = dt %>%
  arrange(ts.avg) %>%
  arrange(trip_id)
```


# Timetable

Without considering delays, we display a timetableforall trains between Genevaand Lausanne

## Build
Group the stops measure by trip and stop, and summarise delta arrival taking the meadian

```{r timetable-subset}

dt.timetable=dt %>%
  group_by(segment.tag,
           transport_type,
           station_name,
           trip_distance,
           day.type,
           h.arrival.time,
           h.departure.time,
           next.station_name,
           next.trip_distance,
           next.h.arrival.time,
           next.h.departure.time,
           frequency,
           segment.tag
  ) %>%
  summarize(
    timetable.day.from = min(day),
    timetable.day.to = max(day),
    median.next.delta.arrival.min=median(next.delta.arrival.min, na.rm = TRUE),
    q80.next.delta.arrival.min=quantile(next.delta.arrival.min, prob=0.8, na.rm = TRUE)
  ) %>%
  arrange(frequency)

dt.timetable =subset(dt.timetable, ! is.na(next.station_name))
```

### Merging slightly shifted trains

Some Schedules have slightly being shifted during the studied period. For example, the Nyon-Coppet segment left Nyon at 7:54 and 7:55. In order to get a better better aggregated titmetable, thes two segment shall be merged.

We also have situation where the departure/arrival is the same, but the differntiator is the time the train will be leaving the station

```{r}
# dt.nc = dt %>%
#   filter(
#     station_name == 'Allaman' &
#       next.station_name == 'Morges' &
#       h.departure.time > 7.5*60 &
#       h.departure.time < 8*60 
#   ) %>%
#   select(
#     day.trip.id,
#     day.type,
#     station_name,
#     next.station_name,
#     h.departure.time,
#     next.h.arrival.time,
#     segment.tag,
#     frequency
#   )
# dt.nc.tt = as.data.frame(dt.timetable) %>%
#   filter(
#     station_name == 'Allaman' &
#       next.station_name == 'Morges' &
#       h.departure.time > 7.5*60 &
#       h.departure.time < 8*60 
#   ) %>%
#   select(
#     day.type,
#     station_name,
#     next.station_name,
#     h.departure.time,
#     next.h.arrival.time,
#     segment.tag,
#     frequency,
#     timetable.day.from,
#     timetable.day.to, 
#     distance.to.follower,
#     to.merge.with.previous
#   )
```

```{r timetable-merge-close-segments}
dt.timetable = dt.timetable %>%
  arrange(day.type, station_name, next.station_name, h.departure.time)

#dt.tt.next is a circular shoft of dt.timetable
dt.tt.next = rbind(
  dt.timetable[2:nrow(dt.timetable),],
    dt.timetable[1,]
)

dt.timetable$distance.to.follower = ifelse(
  dt.timetable$day.type == dt.tt.next$day.type &
    dt.timetable$station_name == dt.tt.next$station_name &
    dt.timetable$next.station_name == dt.tt.next$next.station_name,
  pmax(
    abs(dt.timetable$h.departure.time - dt.tt.next$h.departure.time),
    abs(dt.timetable$next.h.arrival.time - dt.tt.next$next.h.arrival.time)
  ),
  Inf
)


dt.timetable$frequency.follower=c(dt.timetable$frequency[2:nrow(dt.timetable)], 0)
dt.timetable$frequency =ifelse (
  dt.timetable$distance.to.follower>2, 
  dt.timetable$frequency,
  dt.timetable$frequency + dt.timetable$frequency.follower
  )


dt.timetable$to.merge.with.previous = c(
  FALSE,
  dt.timetable$distance.to.follower[1:nrow(dt.timetable)-1]<=2
  )

dt.timetable.merged = dt.timetable[1,]
i.tt.merged = 1
for (i.tt in 2:nrow(dt.timetable)){
  print(i.tt)
  to.merge=dt.timetable$to.merge.with.previous[i.tt]
  if(!is.na(to.merge) & to.merge){
    dt.timetable.merged[i.tt.merged,]$frequency = 
      dt.timetable.merged[i.tt.merged,]$frequency +
            dt.timetable[i.tt,]$frequency 
  }else{
    dt.timetable.merged = rbind(
      dt.timetable.merged,
      dt.timetable[i.tt,]
    )
    i.tt.merged = i.tt.merged +1
  }
}
dt.timetable = as.data.frame(dt.timetable.merged)

dt.timetable$to.merge=NULL
dt.timetable$distance.to.follower=NULL
dt.timetable$to.merge.with.previous=NULL
dt.timetable$frequency.follower=NULL

```



#### Remove trips with little frequency (<5%)

```{r actual-trip-remove-low-frequency}

#dt.timetable = subset(dt.timetable, frequency >=0.05)

```



### Add train direction
Extract head and tail stations. Then if `trip_distance < next.trip_distance` that means that te trip is flowing forward.

```{r timetable-add-direction}
station.head = as.character(
  dt.timetable[dt.timetable$trip_distance == 0, ]$station_name[1]
)
dist.max = max(dt.timetable$trip_distance)
station.tail = as.character(
  dt.timetable[dt.timetable$trip_distance == dist.max,]$station_name[1]
)

dt.timetable$trip.description = ifelse(
  dt.timetable$trip_distance < dt.timetable$next.trip_distance,
  paste(station.head, station.tail,sep='-'),
  paste(station.tail, station.head,sep='-')
)
```

# Saving

## timetable tsv
The file is simply saved as tsv into `r timetable.out.filename`

```{r save-timetable}
write.table(
   dt.timetable,
   file = timetable.out.filename,
   sep='\t',
   row.names = FALSE,
   quote = FALSE
)
```

```{r save-ist-clean}

write.table(
   dt,
   file = trip.ist.clean.out.filename,
   sep='\t',
   row.names = FALSE,
   quote = FALSE
)
```


## Metadata

Some overall data, such as the considered time period, is not contained in the aggregated timetable.
```{r save-metadata}

l.metadata = list(
  period_date_from = period.date.from,
  period_date_to = period.date.to
)
json.metadata = toJSON(l.metadata)
write(json.metadata, file=metadata.out.filename)

```

Et voilà, you are ready to go and play with the aggregation
