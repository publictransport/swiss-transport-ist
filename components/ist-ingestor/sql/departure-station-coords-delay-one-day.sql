SELECT EXTRACT(EPOCH FROM sm.operating_day) as operating_day,
       sm.is_cancelled as is_cancelled,
       sm.is_additional_driving as is_additional_driving,
       sm.company_name as company_name,
       EXTRACT(EPOCH FROM sm.arrival_time) as arrival_time_0,
       EXTRACT(EPOCH FROM sm.arrival_actual) as arrival_actual_0,
       EXTRACT(EPOCH FROM sm.departure_time )as departure_time_0,
       EXTRACT(EPOCH FROM sm.departure_actual) as departure_actual_0,
       stop.name  as station_name_0,
       stop.coords -> 'Lat' as lat_0,
       stop.coords -> 'Lon' as lon_0,
       EXTRACT(EPOCH FROM next_sm.arrival_time) as arrival_time_1,
       EXTRACT(EPOCH FROM next_sm.arrival_actual) as arrival_actual_1,
       next_stop.name  as station_name_1,
       next_stop.coords -> 'Lat' as lat_1,
       next_stop.coords -> 'Lon' as lon_1,
       EXTRACT(EPOCH FROM (next_sm.arrival_actual - next_sm.arrival_time)) as arrival_delta_seconds_1
FROM stop_measures AS sm
   INNER JOIN stop_measures as next_sm
      ON next_sm.id = sm.next_stop_measure_id
   INNER JOIN stops stop
      ON sm.station_id = stop.id
   INNER JOIN stops next_stop
      ON next_sm.station_id = next_stop.id
WHERE
    /*sm.operating_day = '2018-12-13 00:00:00+00'*/
    /*sm.operating_day = '2018-03-01 00:00:00+00'*/
   sm.operating_day = '2018-05-20 00:00:00+00'
  /*  AND sm.company_name = 'Schweizerische Bundesbahnen SBB' */
  /*AND NOT next_sm.arrival_time IS NULL
  AND NOT next_sm.arrival_actual IS NULL*/
