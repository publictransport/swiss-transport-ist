/* count the number of trains with delay greater than 5 minutes per day/hour */
SELECT operating_day,dep_hour,COUNT(*) as n FROM
        (SELECT
        operating_day,
        EXTRACT( HOUR FROM departure_time) as dep_hour,
        EXTRACT( EPOCH FROM (departure_actual-departure_time))  as delta
        FROM stop_measures) s
    WHERE
       delta > 300
    GROUP BY
       operating_day,dep_hour ORDER BY n DESC;