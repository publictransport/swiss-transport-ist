SELECT operating_day,
       arrival_hour,
       arrival_minute,
       COUNT(*) as n_tot,
       SUM(delay_3_min) as n_3_min,
       SUM(delay_5_min) as n_5_min,
       SUM(delay_10_min) as n_10_min,
       SUM(cancelled) as n_cancelled
FROM
  (SELECT operating_day,
          EXTRACT(HOUR FROM arrival_time)                     as arrival_hour,
          EXTRACT(MINUTE FROM arrival_time)                   as arrival_minute,
          EXTRACT(EPOCH FROM (arrival_actual - arrival_time)) as delta,
          CASE
            WHEN EXTRACT(EPOCH FROM (arrival_actual - arrival_time)) >= 3*60 THEN 1
            ELSE 0
            END AS delay_3_min,
          CASE
            WHEN EXTRACT(EPOCH FROM (arrival_actual - arrival_time)) >= 5*60 THEN 1
            ELSE 0
            END AS delay_5_min,
          CASE
            WHEN EXTRACT(EPOCH FROM (arrival_actual - arrival_time)) >= 10*60 THEN 1
            ELSE 0
            END AS delay_10_min,
          CASE
            WHEN is_cancelled THEN 1
            ELSE 0
            END AS cancelled
   FROM stop_measures) s
WHERE
    operating_day = '2018-03-01 00:00:00+00'
GROUP BY operating_day, arrival_hour, arrival_minute
ORDER BY arrival_hour ASC,
         arrival_minute ASC
