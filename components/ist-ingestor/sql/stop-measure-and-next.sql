SELECT (stop.name, next_stop.name)
FROM stop_measures as sm
  INNER JOIN stop_measures as next_sm
    ON next_sm.id = sm.next_stop_measure_id
  INNER JOIN stops stop
    ON sm.station_id = stop.id
  INNER JOIN stops next_stop
    ON next_sm.station_id = next_stop.id
WHERE stop.name = 'Nyon'