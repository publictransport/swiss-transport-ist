SELECT operating_day,
       arrival_time,
       stop.name station_name,
       stop.coords -> 'Lat' as lat,
       stop.coords -> 'Lat' as lon
FROM stop_measures AS sm
       INNER JOIN stops AS stop
                  ON  sm.station_id = stop.id
WHERE
    operating_day = '2018-03-01 00:00:00+00'
  AND is_cancelled
