/* count the number of trains with delay greater than 5 minutes per day */
SELECT operating_day,COUNT(*) as n FROM
        (SELECT
        operating_day,
        EXTRACT( EPOCH FROM (departure_actual-departure_time))  as delta
        FROM stop_measures) s
    WHERE
       delta > 300
    GROUP BY
       operating_day ORDER BY n DESC;