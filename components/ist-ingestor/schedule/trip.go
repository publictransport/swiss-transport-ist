package schedule

import (
	"errors"
	"fmt"
	"log"
)

// A Trip here is simply a set of mockStations and the list of them
type Trip struct {
	Id        string
	StopIdSet map[string]bool
	Stops     []*Stop
}

// NewTrip helper to instantiate a Trip object
func NewTrip(id string, stops []*Stop) *Trip {
	set := make(map[string]bool)
	list := make([]*Stop, 0)
	for _, s := range stops {
		set[s.Id] = true
		list = append(list, s)
	}
	return &Trip{
		Id:        id,
		StopIdSet: set,
		Stops:     list,
	}
}

// IsPassingThrough is true if the trip goes through the given station
func (l *Trip) IsPassingThrough(station *Stop) bool {
	if station == nil {
		log.Panic("station is nil")
	}
	_, ok := l.StopIdSet[station.Id]
	return ok
}
func (l *Trip) Size() int {
	return len(l.Stops)
}

func (l *Trip) GetBounds() (Bound, error) {
	if l.Size() == 0 {
		return Bound{nil, nil}, errors.New("Trip should at least have one stop in order to extract Bound")
	}
	return Bound{l.Stops[0], l.Stops[l.Size()-1]}, nil
}

// Head first station
func (l *Trip) Head() *Stop {
	if l.Size() == 0 {
		return nil
	}
	return l.Stops[0]
}

//Tail last station
func (l *Trip) Tail() *Stop {
	if l.Size() == 0 {
		return nil
	}
	return l.Stops[l.Size()-1]
}

func (l *Trip) String() string {
	s := fmt.Sprintf("id=%v ", l.Id)

	s += fmt.Sprintf("stations=%v", l.Stops)
	return s
}

//Bound two Stations
type Bound struct {
	Stop1 *Stop
	Stop2 *Stop
}

func (b Bound) Reverse() Bound {
	return Bound{b.Stop2, b.Stop1}
}
