package schedule

import (
	"log"
	"reflect"
	"testing"
)

func buildMockStationDict() (dict *StopDict) {
	dict = NewStopDict()
	dict.Put(&Stop{Id: "a", Name: "aaa"})
	dict.Put(&Stop{Id: "b", Name: "bbb"})
	dict.Put(&Stop{Id: "c", Name: "ccc"})
	dict.Put(&Stop{Id: "d", Name: "ddd"})
	dict.Put(&Stop{Id: "e", Name: "eee"})
	dict.Put(&Stop{Id: "f", Name: "fff"})
	dict.Put(&Stop{Id: "g", Name: "ggg"})
	dict.Put(&Stop{Id: "h", Name: "hhh"})
	return dict
}

var mockStations = buildMockStationDict()

func getMockStations(id string) *Stop {
	s, err := mockStations.Get(id)
	if err != nil {
		log.Panic(err)
	}
	return s
}

func getMockStationsList(ids []string) []*Stop {
	xs, err := mockStations.GetFromList(ids)
	if err != nil {
		log.Panic(err)
	}
	return xs
}

func TestNewTrip(t *testing.T) {
	type args struct {
		id       string
		stations []*Stop
	}
	tests := []struct {
		name string
		args args
		want *Trip
	}{
		{
			"empty",
			args{"A", []*Stop{}},
			&Trip{
				Id:        "A",
				Stops:     []*Stop{},
				StopIdSet: map[string]bool{},
			},
		},
		{
			"some",
			args{"A", getMockStationsList([]string{"a", "c", "b"})},
			&Trip{
				Id:    "A",
				Stops: getMockStationsList([]string{"a", "c", "b"}),
				StopIdSet: map[string]bool{
					"a": true, "b": true, "c": true,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewTrip(tt.args.id, tt.args.stations); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewTrip() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetBound(t *testing.T) {
	l := NewTrip("xx", getMockStationsList([]string{"a", "c", "b"}))
	b, _ := l.GetBounds()
	want := Bound{getMockStations("a"), getMockStations("b")}
	if b != want {
		t.Errorf("Bound: got=%v  want=%v", b, want)
	}
}
