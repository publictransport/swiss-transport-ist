package schedule

import (
	"fmt"
	"octo.ch/swiss-transport-ist/geo"
)

//parsing the GTFS schedul info

type Route struct {
	Id          string
	Description string
	Type        int
}

func (r Route) IsTrain() bool {
	return (r.Type >= 100 && r.Type < 200) || (r.Type >= 400 && r.Type < 500)
}

type Stop struct {
	tableName struct{} `sql:"stops"`
	Id        string   `sql:",pk"`
	Name      string
	Coords    geo.Coordinates
}

func (s *Stop) String() string {
	return fmt.Sprintf("(%v) %v", s.Id, s.Name)
}

type TripDescription struct {
	Id      string
	RouteId string
}
