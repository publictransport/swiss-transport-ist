package schedule

import (
	"fmt"
	"sync"
)

type StopDict struct {
	store map[string]*Stop
	//	name2id map[string]string
	mutex *sync.Mutex
}

func NewStopDict() *StopDict {
	return &StopDict{
		store: make(map[string]*Stop),
		//		name2id: make(map[string]string),
		mutex: &sync.Mutex{},
	}
}

//Put store a station in the dictionary, using its id as a store key.
// If another station with the same key is already existing, it will be returned
// throw error if we try to store s Stop with the same id but a different name
func (dict *StopDict) Put(station *Stop) (*Stop, error) {
	dict.mutex.Lock()
	defer dict.mutex.Unlock()

	//refId := station.Id
	//var err error
	//sDup := dict.FindByName(station.Name)
	//if sDup != nil && sDup.Id != station.Id {
	//	refId, err = reduceId(station.Id, sDup.Id)
	//	if (err != nil) {
	//		return nil, incompatibleStopIdsWithSameNamesError{station.Name, station.Id, sDup.Id}
	//	}
	//}

	s, ok := dict.store[station.Id]
	if ok && station.Name != s.Name {
		return nil, duplicateStopEntriesWithDifferentNamesError{s, station}
	}
	if ok {
		return s, nil
	}
	dict.store[station.Id] = station
	//	dict.name2id[station.Name] = refId
	return station, nil
}

func (dict *StopDict) Get(id string) (*Stop, error) {
	s, ok := dict.store[id]
	if !ok {
		return nil, stopIdNotFoundError{id, dict}
	}
	return s, nil
}
func (dict *StopDict) GetFromList(ids []string) (stations []*Stop, err error) {
	stations = make([]*Stop, 0)
	for _, id := range ids {
		s, err := dict.Get(id)
		if err != nil {
			return nil, err
		}
		stations = append(stations, s)
	}
	return
}

func (dict *StopDict) List() (l []*Stop) {
	l = make([]*Stop, 0)
	for _, s := range dict.store {
		l = append(l, s)
	}
	return
}

func (dict *StopDict) Size() int {
	return len(dict.store)
}

type duplicateStopEntriesWithDifferentNamesError struct {
	s1 *Stop
	s2 *Stop
}

func (e duplicateStopEntriesWithDifferentNamesError) Error() string {
	return fmt.Sprintf("ERROR duplicate station ID With different names id=%v [%v] [%v]", e.s1.Id, e.s1.Name, e.s2.Name)
}

type incompatibleStopIdsWithSameNamesError struct {
	name string
	id1  string
	id2  string
}

func (e incompatibleStopIdsWithSameNamesError) Error() string {
	return fmt.Sprintf("ERROR incompatible station ID with same name=%v [%v] [%v]", e.name, e.id1, e.id2)
}

type stopIdNotFoundError struct {
	id string
	dict *StopDict
}

func (e stopIdNotFoundError) Error() string {
	return fmt.Sprintf("stopIdNotFoundError [%v] among %v", e.id, e.dict.Size())
}

type RouteDict map[string]*Route

func NewRouteDict() RouteDict {
	return RouteDict{}
}

func (d *RouteDict) Contains(id string) bool {
	_, ok := (*d)[id]
	return ok
}

func (d *RouteDict) Add(r *Route) {
	(*d)[r.Id] = r
}

func (d *RouteDict) Size() int {
	return len(*d)
}

// TripDescription based on GTFS/trips.txt, gather a trip description (here only the route id
type TripDescriptionDict map[string]*TripDescription

func (d *TripDescriptionDict) Contains(id string) bool {
	_, ok := (*d)[id]
	return ok
}

func (d *TripDescriptionDict) Add(r *TripDescription) {
	(*d)[r.Id] = r
}

func (d *TripDescriptionDict) Size() int {
	return len(*d)
}
