package schedule

import (
	"testing"

	"octo.ch/swiss-transport-ist/geo"
)

func mockStop(x string) *Stop {
	return &Stop{
		Id:     x,
		Name:   x + x + x,
		Coords: geo.Coordinates{0, 0},
	}
}

func TestStopDict_StopDict_Get_notFound(t *testing.T) {
	dict := NewStopDict()

	s, err := dict.Get("a")
	if err == nil {
		t.Error("when not found, error shall not be nil")
	}
	if s != nil {
		t.Errorf("shall return nil")
	}

}
func TestStopDict_StopDict_PutGet_notFound(t *testing.T) {
	dict := NewStopDict()

	dict.Put(mockStop("b"))
	s, err := dict.Get("b")
	if err != nil {
		t.Error(err)
	}
	if s == nil {
		t.Errorf("shall return nil %v", s)
	}

}

func TestStopDict_StopDict_PutGet_found(t *testing.T) {
	dict := NewStopDict()

	dict.Put(mockStop("a"))
	dict.Put(mockStop("b"))
	s, err := dict.Get("a")
	if err != nil {
		t.Error(err)
	}
	want := mockStop("a")
	if *s != *want {
		t.Errorf("got %v, want %v", s, want)
	}

}
