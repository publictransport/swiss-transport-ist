
## SQL 


### How late are the 8:14 train leaving from Nyon
Hint: we are targeting here train from Nyon, but toward Geneva and Moregs. Duh,,,
But we also now have the nextStation to refine this


    SELECT to_char(m.operating_day,'DD/MM/YYY'),
           m.line_id,
           m.company_name,
           m.transport_type,
           to_char(m.departure_time,'HH:MI:SS') as dep_t,
           m.departure_actual,
           EXTRACT(EPOCH FROM (m.departure_actual - m.departure_time)),
           m.is_cancelled,
           s.name 
    FROM stop_measures as m 
    INNER JOIN stations as s 
          ON m.station_id = s.id 
    WHERE s.name = 'Nyon' AND 
          to_char(m.departure_time,'HH:MI:SS') = '08:14:00';
          


### Worst hour

    SELECT operating_day,dep_hour,COUNT(*) as n FROM
        (SELECT operating_day, EXTRACT( HOUR FROM departure_time) as dep_hour,EXTRACT( EPOCH FROM (departure_actual-departure_time))  as delta FROM stop_measures) s 
    WHERE 
       delta > 300 
    GROUP BY 
       operating_day,dep_hour ORDER BY n DESC;
