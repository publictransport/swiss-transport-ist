package ist

import (
	"fmt"
	"sort"
)

type TripWithStopMeasures struct {
	TripId        string
	StopsMeasures []*StopMeasure
}

func NewTripWithStopMeasures(tripId string) TripWithStopMeasures {
	return TripWithStopMeasures{
		TripId:        tripId,
		StopsMeasures: []*StopMeasure{},
	}
}
// Size, the number of StopMeasure
func (t *TripWithStopMeasures) Size()int {
	return len(t.StopsMeasures)
}

// Append add a new StopMeasure to the trip
//return the same object
func (t *TripWithStopMeasures) Append(sm *StopMeasure)*TripWithStopMeasures {
	t.StopsMeasures = append(t.StopsMeasures, sm)
	return t
}
// Signature return a text signature for the trip, based on stations and departure times
func (t *TripWithStopMeasures) Signature() string {
	s:=""
	t.sort()
	for _, sm := range t.StopsMeasures{
		if(s!=""){
			s=s+"|"
		}
		s+=fmt.Sprintf("%v@%v", sm.Station.Name, sm.DepartureTime)
	}
	return s
}

//IsCancelled return true or false is all StopMeasures are either true or false
// returns EmptyTripWithStopMeasuresError is the trip happens to be empty
// returns IncoherentCancellationError is not all the value are the same
func (t *TripWithStopMeasures) IsFullyCancelled() (bool, error) {
	if t.Size() == 0 {
		return false, &EmptyTripWithStopMeasuresError{t}
	}
	acc := t.StopsMeasures[0].IsCancelled

	for _, sm:= range(t.StopsMeasures){
		if sm.IsCancelled != acc {
			return false, &IncoherentCancellationError{t}
		}
	}
	return acc, nil
}

//internal function to sort StopMeasure by departure time
func (t *TripWithStopMeasures) sort() {
	sort.Slice(
		t.StopsMeasures,
		func(i, j int) bool {
			a := t.StopsMeasures[i].DepartureTime
			b := t.StopsMeasures[j].DepartureTime
			if a == nil {
				return false
			}
			if b == nil {
				return true
			}
			return a.Before(*b)
		},
	)
}

type IncoherentCancellationError struct{
	t *TripWithStopMeasures
}

func (e *IncoherentCancellationError)Error() string{
	return fmt.Sprintf("IncoherentCancellationError [%v]", e.t.TripId)
}

type EmptyTripWithStopMeasuresError struct{
	t *TripWithStopMeasures
}
func (e *EmptyTripWithStopMeasuresError)Error() string{
	return fmt.Sprintf("EmptyTripWithStopMeasuresError [%v]", e.t.TripId)
}