package ist

import (
	"testing"
	"time"

	"octo.ch/swiss-transport-ist/schedule"
)

func TestTripWithStopMeasures_Signature(t *testing.T) {
	tA, _ := time.Parse("02.01.2006 15:04", "12.10.2018 15:34")
	smA := &StopMeasure{Station: &schedule.Stop{Name: "a"}, DepartureTime: &tA}
	tB, _ := time.Parse("02.01.2006 15:04", "12.10.2018 15:40")
	smB := &StopMeasure{Station: &schedule.Stop{Name: "b"}, DepartureTime: &tB}
	tC, _ := time.Parse("02.01.2006 15:04", "12.10.2018 15:45")
	smC := &StopMeasure{Station: &schedule.Stop{Name: "c"}, DepartureTime: &tC}
	smD := &StopMeasure{Station: &schedule.Stop{Name: "d"}, DepartureTime: nil}

	type fields struct {
		TripId        string
		StopsMeasures []*StopMeasure
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			"empty",
			fields{"0", []*StopMeasure{}},
			"",
		},
		{
			"ordered full",
			fields{"0", []*StopMeasure{smA, smB, smC, smD}},
			"a@2018-10-12 15:34:00 +0000 UTC|b@2018-10-12 15:40:00 +0000 UTC|c@2018-10-12 15:45:00 +0000 UTC|d@<nil>",
		},
		{
			"disordered full",
			fields{"0", []*StopMeasure{smA, smD, smC, smB}},
			"a@2018-10-12 15:34:00 +0000 UTC|b@2018-10-12 15:40:00 +0000 UTC|c@2018-10-12 15:45:00 +0000 UTC|d@<nil>",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			twsm := &TripWithStopMeasures{
				TripId:        tt.fields.TripId,
				StopsMeasures: tt.fields.StopsMeasures,
			}
			if got := twsm.Signature(); got != tt.want {
				t.Errorf("TripWithStopMeasures.Signature() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTripWithStopMeasures_Append_Size(t *testing.T) {
	twsm:=NewTripWithStopMeasures("1")
	twsm.Append(&StopMeasure{}).Append(&StopMeasure{}).Append(&StopMeasure{})
	if twsm.Size() != 3 {
		t.Errorf("expected Size()=3 [%v]", twsm.Size())
	}
}

func TestTripWithStopMeasures_IsFullyCancelled(t *testing.T) {
	type fields struct {
		TripId        string
		StopsMeasures []*StopMeasure
	}
	tests := []struct {
		name    string
		fields  fields
		want    bool
		wantErr bool
	}{
		{
			"empty",
			fields{"empty",[]*StopMeasure{}},
			false,
			true,
		},
		{
			"one",
			fields{"empty",[]*StopMeasure{&StopMeasure{IsCancelled:true}}},
			true,
			false,
		},
		{
			"multiple",
			fields{"empty",[]*StopMeasure{
				&StopMeasure{IsCancelled:false},
				&StopMeasure{IsCancelled:false},
				&StopMeasure{IsCancelled:false},
			}},
			false,
			false,
		},
		{
			"incoherent",
			fields{"empty",[]*StopMeasure{
				&StopMeasure{IsCancelled:false},
				&StopMeasure{IsCancelled:false},
				&StopMeasure{IsCancelled:true},
			}},
			false,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			twsm := &TripWithStopMeasures{
				TripId:        tt.fields.TripId,
				StopsMeasures: tt.fields.StopsMeasures,
			}
			got, err := twsm.IsFullyCancelled()
			if (err != nil) != tt.wantErr {
				t.Errorf("TripWithStopMeasures.IsFullyCancelled() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("TripWithStopMeasures.IsFullyCancelled() = %v, want %v", got, tt.want)
			}
		})
	}
}
