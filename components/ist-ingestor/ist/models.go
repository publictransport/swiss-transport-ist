package ist

import (
	"fmt"
	"octo.ch/swiss-transport-ist/schedule"
	"time"
)

type StopMeasure struct {
	tableName           struct{} `sql:"stop_measures"`
	Id                  uint64      `sql:",pk"`
	OperatingDay        time.Time
	TripId              string
	CompanyId           string
	CompanyName         string
	Product             string
	LineId              string
	LineText            string
	TransportType       string
	IsAdditionalDriving bool `sql:",notnull"`
	IsCancelled         bool `sql:",notnull"`
	StationId           string //`sql:",fk"`
	Station             *schedule.Stop
	NextStopMeasureId	uint64
	NextStopMeasure		*StopMeasure
	ArrivalTime         *time.Time
	ArrivalActual       *time.Time
	DepartureTime       *time.Time
	DepartureActual     *time.Time
}

func formatTime(t *time.Time) string {
	if t == nil {
		return ""
	}
	return t.Format("15:04:05")
}

func (s *StopMeasure) String() string {
	return fmt.Sprintf("%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\t(%v) %v",
		s.OperatingDay.Format("02.01.2006"),
		s.TripId,
		s.LineText,
		s.TransportType,
		s.IsAdditionalDriving,
		s.IsCancelled,
		formatTime(s.ArrivalTime),
		formatTime(s.ArrivalActual),
		formatTime(s.DepartureTime),
		formatTime(s.DepartureActual),
		s.Station.Id,
		s.Station.Name,
	)
}
