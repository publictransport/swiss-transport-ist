package parser_commons

import (
	"testing"
)

func Test_areArraysEqual(t *testing.T) {
	type args struct {
		actual   []string
		expected []string
	}

	tests := []struct {
		name        string
		args        args
		wantBool    bool
		wantMessage string
	}{
		{
			name:        "empty",
			args:        args{[]string{}, []string{}},
			wantBool:    true,
			wantMessage: "",
		},
		{
			name:        "same",
			args:        args{[]string{"ab", "cd", "ef"}, []string{"ab", "cd", "ef"}},
			wantBool:    true,
			wantMessage: "",
		},
		{
			name:        "different length",
			args:        args{[]string{"ab", "cd", "ef"}, []string{"ab", "cd"}},
			wantBool:    false,
			wantMessage: "length are different 3/2",
		},
		{
			name:        "different el first",
			args:        args{[]string{"ab", "cd", "ef"}, []string{"xy", "cd", "ef"}},
			wantBool:    false,
			wantMessage: "element differ at position 0: [ab]/[xy]",
		},
		{
			name:        "different el last",
			args:        args{[]string{"ab", "cd", "ef"}, []string{"ab", "cd", "xy"}},
			wantBool:    false,
			wantMessage: "element differ at position 2: [ef]/[xy]",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, gotMessage := areArraysEqual(tt.args.actual, tt.args.expected)
			if got != tt.wantBool {
				t.Errorf("areArraysEqual() bool: actual %v, want %v", got, tt.wantBool)
			}
			if gotMessage != tt.wantMessage {
				t.Errorf("areArraysEqual() message: actual '%v', want '%v'", gotMessage, tt.wantMessage)
			}
		})
	}
}
