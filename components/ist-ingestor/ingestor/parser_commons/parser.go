package parser_commons

import (
	"encoding/csv"
	"errors"
	"fmt"
	"os"
	"regexp"
	"strings"
)

type Reader struct {
	HeaderRefLine string
	HeaderSep     rune
}

// ReadRecordsFromFile parse a CSV file, check the header line to be sas expected and return a list of line, split in item.
func (r *Reader) ReadRecordsFromFile(filename string) ([][]string, error) {
	csvFile, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	rcsv := csv.NewReader(csvFile)
	rcsv.Comma = r.HeaderSep

	records, err := r.readRecordsFromCSVReader(rcsv)
	if err != nil {
		return nil, err
	}

	return records, nil
}

func (r *Reader) readRecordsFromCSVReader(rcsv *csv.Reader) ([][]string, error) {
	records, err := rcsv.ReadAll()
	if err != nil {
		return nil, err
	}

	header, records := records[0], records[1:]
	err = r.checkHeaderLine(header)
	if err != nil {
		return nil, err
	}
	return records, nil
}

// checkHeaderLine panics if the file header line is not as expected. As we rely on that for unmarshalling CSV, we better check it out
func (r *Reader) checkHeaderLine(header []string) error {
	re := regexp.MustCompile("^\\W")
	header[0] = re.ReplaceAllLiteralString(header[0], "")

	same, msg := areArraysEqual(header, strings.Split(r.HeaderRefLine, string(r.HeaderSep)))

	if same {
		return nil
	}
	return errors.New(fmt.Sprintf("file header is not as expected: %v", msg))
}

// areArraysEqual compare two string array and issue a boolean for them to match. An explicit error message is reported in case of misalignment
func areArraysEqual(actual []string, expected []string) (bool, string) {
	if len(actual) != len(expected) {
		return false, fmt.Sprintf("length are different %v/%v", len(actual), len(expected))
	}
	for i, _ := range actual {
		if actual[i] != expected[i] {
			return false, fmt.Sprintf("element differ at position %v: [%v]/[%v]", i, actual[i], expected[i])
		}
	}
	return true, ""
}
