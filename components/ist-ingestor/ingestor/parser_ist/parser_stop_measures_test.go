package parser_ist

import (
	"fmt"
	"github.com/go-test/deep"
	"log"
	"octo.ch/swiss-transport-ist/ingestor/gtfs"
	"octo.ch/swiss-transport-ist/schedule"
	"reflect"
	"strings"
	"testing"
	"time"

	"octo.ch/swiss-transport-ist/ist"
)

func Test_recordsFromFile(t *testing.T) {
	type args struct {
		filename string
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			name:    "fine",
			args:    args{"../data_test/ist-100-ok.csv"},
			want:    120,
			wantErr: false,
		},
		{
			name:    "corrutped first char  header",
			args:    args{"../data_test/ist-100-first-char-corrupted-header.csv"},
			want:    99,
			wantErr: false,
		},
		{
			name:    "tuncated trip",
			args:    args{"../data_test/ist-100-truncated_line.csv"},
			want:    0,
			wantErr: true,
		},
		{
			name:    "wrong header",
			args:    args{"../data_test/ist-100-wrong-header.csv"},
			want:    0,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			stationDict := schedule.NewStopDict()
			reader := NewStopMeasureReader(stationDict)
			got, err := reader.ReadRecordsFromFile(tt.args.filename)
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadRecordsFromFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(got) != tt.want {
				t.Errorf("ReadRecordsFromFile() count lines %v, want %v", len(got), tt.want)
			}
		})
	}
}

func Test_stationAreNotDuplicated(t *testing.T) {
	stationReader := gtfs.NewStopReader()
	stationFilename := "../data_test/stops.txt"
	stationDict, err := stationReader.ReadFromCSVFile(stationFilename)
	if err != nil {
		t.Error(err)
	}

	reader := NewStopMeasureReader(stationDict)
	filename := "../data_test/ist-100-ok.csv"
	enries, err := reader.ReadStopMeasuresFromCSVFile(filename)
	if err != nil {
		t.Error(err)
	}

	sm1 := enries[0]
	sm2 := enries[13]
	if sm1.Station.Name != sm2.Station.Name {
		t.Errorf("the two (hand) picked station shaould have the same name [%v] [%v] / FIX the test set", sm1.Station.Name, sm2.Station.Name)
	}
	if sm1.Station != sm2.Station {
		t.Errorf("the two (hand) picked station shaould have the same address [%v]->(%v) [%v]->(%v", sm1.Station.Name, &sm1, sm2.Station.Name, &sm2)
	}
}

func Test_parseFieldDateHMS(t *testing.T) {
	type args struct {
		s string
	}
	t1 := time.Date(2018, 4, 1, 7, 30, 36, 0, time.UTC)
	tNil := time.Date(1, 1, 1, 0, 0, 0, 0, time.UTC)

	tests := []struct {
		name    string
		args    args
		want    *time.Time
		wantErr bool
	}{
		{
			name:    "good",
			args:    args{"01.04.2018 07:30:36"},
			want:    &t1,
			wantErr: false,
		},
		{
			name:    "empty give nil",
			args:    args{""},
			want:    nil,
			wantErr: false,
		},
		{
			name:    "bad format",
			args:    args{"01.04.2018 07:30"},
			want:    &tNil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseFieldDateHMS(tt.args.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseFieldDateHMS() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseFieldDateHMS() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_newIstEntry(t *testing.T) {
	type args struct {
		recs []string
	}
	t0depTime := time.Date(2018, 1, 1, 5, 45, 0, 0, time.UTC)
	t0depActual := time.Date(2018, 1, 1, 5, 46, 0, 0, time.UTC)

	stationDict := schedule.NewStopDict()
	stationDict.Put(&schedule.Stop{Id: "8500090", Name: "Basel Bad Bf"})
	station, err := stationDict.Get("8500090")
	if err != nil {
		log.Panic(err)
	}

	tests := []struct {
		name    string
		args    args
		want    *ist.StopMeasure
		wantErr bool
	}{
		{
			name: "straight",
			args: args{recs: strings.Split("01.01.2018;80:06____:17010:000;80:06____;DB;DB Regio AG;train;17010;RE;;RE;false;true;8500090;Basel Bad Bf;;;forecast;01.01.2018 05:45;01.01.2018 05:46:00;forecast;false", ";")},
			want: &ist.StopMeasure{
				OperatingDay:        time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC),
				TripId:              "80:06____:17010:000",
				CompanyId:           "80:06____",
				CompanyName:         "DB Regio AG",
				Product:             "train",
				LineId:              "17010",
				LineText:            "RE",
				TransportType:       "RE",
				IsAdditionalDriving: false,
				IsCancelled:         true,
				StationId:           "8500090",
				Station:             station,
				ArrivalTime:         nil,
				ArrivalActual:       nil,
				DepartureTime:       &t0depTime,
				DepartureActual:     &t0depActual,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			reader := NewStopMeasureReader(stationDict)
			got, err := reader.newStopMeasureFromCSVLine(tt.args.recs)
			if (err != nil) != tt.wantErr {
				t.Errorf("newStopMeasureFromCSVLine() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			diff := deep.Equal(got, tt.want)
			if diff != nil {
				t.Errorf(`newStopMeasureFromCSVLine() 
got  %v
want %v`, got, tt.want)
				t.Errorf("%v", diff)
			}
		})
	}
}

func Benchmark_ReadEntriesFromCSV(b *testing.B) {
	cvsFilename := "/Users/alex/tmp/ist-10k.csv"
	//cvsFilename := "/Users/alex/dev/bigdata/cff-data/realtime-dumps/data/translated/2018-01-09istdaten-en.csv"
	for i := 0; i < b.N; i++ {
		stationDict := schedule.NewStopDict()
		reader := NewStopMeasureReader(stationDict)
		entries, err := reader.ReadStopMeasuresFromCSVFile(cvsFilename)
		if err != nil {
			log.Panic(err)
		}
		log.Println(fmt.Sprintf("processed %v entries", len(entries)))
	}
}
