package parser_ist

import (
	"octo.ch/swiss-transport-ist/schedule"
	"reflect"
	"testing"

	"octo.ch/swiss-transport-ist/ist"
)

func Test_filterTrain(t *testing.T) {
	type args struct {
		in []*ist.StopMeasure
	}
	tests := []struct {
		name    string
		args    args
		wantOut []*ist.StopMeasure
	}{
		{
			name:    "none",
			args:    args{[]*ist.StopMeasure{&ist.StopMeasure{TripId: "a", Product: "bus"}}},
			wantOut: []*ist.StopMeasure{},
		},
		{
			name:    "one, keep",
			args:    args{[]*ist.StopMeasure{&ist.StopMeasure{TripId: "a", Product: "train"}}},
			wantOut: []*ist.StopMeasure{&ist.StopMeasure{TripId: "a", Product: "train"}},
		},
		{
			name: "three, keep two",
			args: args{
				[]*ist.StopMeasure{
					&ist.StopMeasure{TripId: "a", Product: "train"},
					&ist.StopMeasure{TripId: "b", Product: "bud"},
					&ist.StopMeasure{TripId: "c", Product: "train"},
				},
			},
			wantOut: []*ist.StopMeasure{
				&ist.StopMeasure{TripId: "a", Product: "train"},
				&ist.StopMeasure{TripId: "c", Product: "train"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotOut := filterTrain(tt.args.in); !reflect.DeepEqual(gotOut, tt.wantOut) {
				t.Errorf("filterTrain() = %v, want %v", gotOut, tt.wantOut)
			}
		})
	}
}

func mapStationName(vs []*ist.StopMeasure) []string {
	vsm := make([]string, len(vs))
	for i, v := range vs {
		vsm[i] = v.Station.Name
	}
	return vsm
}

func Test_filterMultipleTripIdTrips(t *testing.T) {
	type args struct {
		in []*ist.StopMeasure
	}
	stations := map[string]*schedule.Stop{
		"a": &schedule.Stop{Id: "a", Name: "aaa"},
		"b": &schedule.Stop{Id: "b", Name: "bbb"},
		"c": &schedule.Stop{Id: "c", Name: "ccc"},
		"d": &schedule.Stop{Id: "d", Name: "ddd"},
		"e": &schedule.Stop{Id: "e", Name: "eee"},
		"f": &schedule.Stop{Id: "f", Name: "fff"},
		"g": &schedule.Stop{Id: "g", Name: "ggg"},
		"h": &schedule.Stop{Id: "h", Name: "hhh"},
	}
	tests := []struct {
		name    string
		args    args
		wantOut []*ist.StopMeasure
	}{
		{
			name:    "one trip",
			args:    args{[]*ist.StopMeasure{&ist.StopMeasure{TripId: "one"}}},
			wantOut: []*ist.StopMeasure{},
		},
		{
			name: "keep some",
			args: args{
				[]*ist.StopMeasure{
					&ist.StopMeasure{Station: stations["a"], TripId: "one"},
					&ist.StopMeasure{Station: stations["b"], TripId: "two"},
					&ist.StopMeasure{Station: stations["c"], TripId: "two"},
					&ist.StopMeasure{Station: stations["d"], TripId: "two"},
					&ist.StopMeasure{Station: stations["e"], TripId: "three"},
					&ist.StopMeasure{Station: stations["f"], TripId: "four"},
					&ist.StopMeasure{Station: stations["g"], TripId: "five"},
					&ist.StopMeasure{Station: stations["h"], TripId: "five"},
				},
			},
			wantOut: []*ist.StopMeasure{
				&ist.StopMeasure{Station: stations["b"], TripId: "two"},
				&ist.StopMeasure{Station: stations["c"], TripId: "two"},
				&ist.StopMeasure{Station: stations["d"], TripId: "two"},
				&ist.StopMeasure{Station: stations["g"], TripId: "five"},
				&ist.StopMeasure{Station: stations["h"], TripId: "five"},
			},
		},
		{
			name: "all",
			args: args{
				[]*ist.StopMeasure{
					&ist.StopMeasure{Station: stations["b"], TripId: "two"},
					&ist.StopMeasure{Station: stations["c"], TripId: "two"},
					&ist.StopMeasure{Station: stations["d"], TripId: "two"},
				},
			},
			wantOut: []*ist.StopMeasure{
				&ist.StopMeasure{Station: stations["b"], TripId: "two"},
				&ist.StopMeasure{Station: stations["c"], TripId: "two"},
				&ist.StopMeasure{Station: stations["d"], TripId: "two"},
			},
		},
		{
			name: "don't keep last line",
			args: args{
				[]*ist.StopMeasure{
					&ist.StopMeasure{Station: stations["a"], TripId: "two"},
					&ist.StopMeasure{Station: stations["b"], TripId: "two"},
					&ist.StopMeasure{Station: stations["c"], TripId: "two"},
					&ist.StopMeasure{Station: stations["d"], TripId: "three"},
				},
			},
			wantOut: []*ist.StopMeasure{
				&ist.StopMeasure{Station: stations["a"], TripId: "two"},
				&ist.StopMeasure{Station: stations["b"], TripId: "two"},
				&ist.StopMeasure{Station: stations["c"], TripId: "two"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotOut := filterStopMeasureWithSingleTripId(tt.args.in); !reflect.DeepEqual(gotOut, tt.wantOut) {
				t.Errorf(`filterStopMeasureWithSingleTripId() 
got  %v
want %v`, mapStationName(gotOut), mapStationName(tt.wantOut))
			}
		})
	}
}
