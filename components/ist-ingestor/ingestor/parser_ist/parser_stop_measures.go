package parser_ist

import (
	"fmt"
	"log"
	"octo.ch/swiss-transport-ist/ingestor/gtfs"
	"octo.ch/swiss-transport-ist/ingestor/parser_commons"
	"octo.ch/swiss-transport-ist/ist"
	"octo.ch/swiss-transport-ist/schedule"
	"sort"
	"time"
)

type StopMeasureReader struct {
	*parser_commons.Reader
	StationDict *schedule.StopDict
}

func NewStopMeasureReader(stationDict *schedule.StopDict) *StopMeasureReader {
	return &StopMeasureReader{
		Reader: &parser_commons.Reader{
			HeaderRefLine: "operating_day;trip_id;company_id;company_abbr;company_name;product._d;line_id;line_text;circulation_id;transport_type;is_additional_driving;is_cancelled;station_id;station_name;arrival_time;arrival_actual;arrival_actual_status;departure_time;departure_actual;departure_actual_status;is_passing",
			HeaderSep:     ';',
		},
		StationDict: stationDict,
	}
}

// ReadStopMeasuresFromCSVFile parse a CSV file and return a list of StopMeasure
// beside reading CSV, entries are filtered for train and for line ids when multiples entries are present
func (r *StopMeasureReader) ReadStopMeasuresFromCSVFile(filename string) ([]*ist.StopMeasure, error) {
	records, err := r.ReadRecordsFromFile(filename)
	if err != nil {
		return nil, err
	}
	entries := make([]*ist.StopMeasure, 0)
	stationNotFound := make(map[string]int)
	for i, rec := range records {
		if rec[5] != "train" {
			continue
		}
		ie, err := r.newStopMeasureFromCSVLine(rec)

		if err != nil {
			switch err.(type) {
			case gtfs.StopNotFoundError:
				errMsg := fmt.Sprint(err)
				_, ok := stationNotFound[errMsg]
				if !ok {
					stationNotFound[errMsg] = 0
				}
				stationNotFound[errMsg] += 1
				continue
			default:
				log.Println(fmt.Sprintf("Error reading ist.StopMeasure from file [%v] at line [%v]", filename, i+2))
				return nil, err
			}
		}

		entries = append(entries, ie)
	}

	//printMyTrip := func(tag string, sms []*ist.StopMeasure){
	//	log.Println(fmt.Sprintf("########################## %v #####################", tag))
	//	for _, sm := range(sms){
	//		if sm.TripId =="85:11:1726:001"{
	//			log.Println(fmt.Sprintf("%v", sm))
	//		}
	//
	//	}
	//	log.Println("###################################################")
	//}
	//printMyTrip("from csv", entries)
	if len(stationNotFound) > 0 {
		log.Println(fmt.Sprintf("station not found in %v", filename))
		for e, n := range stationNotFound {
			log.Println(fmt.Sprintf("\t%v x %v", n, e))
		}
	}
	linkNextStopMeasure(entries)

	entries = filterTrain(entries)
	//printMyTrip("from filterTrain", entries)
	entries = filterStopMeasureWithSingleTripId(entries)
	//printMyTrip("from filterStopMeasureWithSingleTripId", entries)
	entries = filterDeCancelled(entries)
	//printMyTrip("from filterDeCancelled", entries)
	return entries, nil
}

// newStopMeasureFromCSVLine a ist.StopMeasure constructor from a csv line in block (position do matter)
func (r *StopMeasureReader) newStopMeasureFromCSVLine(recs []string) (*ist.StopMeasure, error) {
	//ie.TripId = strings[1]
	operatingDay, err := parseFieldDay(recs[0])
	if err != nil {
		return nil, err
	}
	arrivalTime, err := parseFieldDateHM(recs[14])
	if err != nil {
		return nil, err
	}
	arrivalActual, err := parseFieldDateHMS(recs[15])
	if err != nil {
		return nil, err
	}
	departureTime, err := parseFieldDateHM(recs[17])
	if err != nil {
		return nil, err
	}
	departureActual, err := parseFieldDateHMS(recs[18])
	if err != nil {
		return nil, err
	}

	station, err := r.StationDict.Get(recs[12])
	if err != nil {
		return nil, gtfs.StopNotFoundError{recs[12], recs[13]}
	}

	entry := ist.StopMeasure{
		OperatingDay:        *operatingDay,
		TripId:              recs[1],
		CompanyId:           recs[2],
		CompanyName:         recs[4],
		Product:             recs[5],
		LineId:              recs[6],
		LineText:            recs[7],
		TransportType:       recs[9],
		IsAdditionalDriving: recs[10] == "true",
		IsCancelled:         recs[11] == "true",
		StationId:           station.Id,
		Station:             station,
		ArrivalTime:         arrivalTime,
		ArrivalActual:       arrivalActual,
		DepartureTime:       departureTime,
		DepartureActual:     departureActual,
	}
	return &entry, nil
}

// linkNextStopMeasure add a pointer to the next station (if there is one)
func linkNextStopMeasure(in []*ist.StopMeasure) {
	// sort byt tripId and ArrivalTime
	sort.Slice(in, func(i, j int) bool {
		a := in[i]
		b := in[j]

		if a.TripId != b.TripId{
			return a.TripId < b.TripId
		}
		if a.ArrivalTime == nil{
			return true
		}
		if b.ArrivalTime == nil{
			return false
		}
		return a.ArrivalTime.Before(*b.ArrivalTime)
	})

	var prevSM *ist.StopMeasure
	for _, sm := range in {
		if prevSM == nil{
			prevSM = sm
			continue
		}
		if prevSM.TripId == sm.TripId{
			prevSM.NextStopMeasure = sm
			prevSM.NextStopMeasureId = sm.Id
		} else {
			prevSM.NextStopMeasure = nil
		}
		prevSM = sm
	}
}

func parseFieldDay(s string) (*time.Time, error) {
	return parseFielTimeFromLayout(s, "02.01.2006")
}
func parseFieldDateHM(s string) (*time.Time, error) {
	return parseFielTimeFromLayout(s, "02.01.2006 15:04")
}

func parseFieldDateHMS(s string) (*time.Time, error) {
	return parseFielTimeFromLayout(s, "02.01.2006 15:04:05")
}

func parseFielTimeFromLayout(s string, layout string) (*time.Time, error) {
	if s == "" {
		return nil, nil
	}
	t, err := time.Parse(layout, s)
	return &t, err
}
