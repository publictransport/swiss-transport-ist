package parser_ist

import (
	"fmt"
	"log"
	"octo.ch/swiss-transport-ist/ist"
)

func add(in []*ist.StopMeasure) (out []*ist.StopMeasure) {
	out = make([]*ist.StopMeasure,0)

	return out
}
// linkNextStopMeasure add a pointer to the next station (if there is one)
func filterTrain(in []*ist.StopMeasure) (out []*ist.StopMeasure) {
	out = []*ist.StopMeasure{}
	for _, e := range in {
		if e.Product == "train" {
			out = append(out, e)
		}
	}
	return
}

// filterStopMeasureWithSingleTripId filter out line that are only matched by  a single tripid
func filterStopMeasureWithSingleTripId(in []*ist.StopMeasure) (out []*ist.StopMeasure) {
	out = []*ist.StopMeasure{}
	var prevSM *ist.StopMeasure = &ist.StopMeasure{
		TripId: "---",
	}
	isPrevOut := false
	for _, e := range in {
		if isPrevOut && e.TripId == prevSM.TripId {
			out = append(out, e)
			continue
		}
		if !isPrevOut && e.TripId == prevSM.TripId {
			out = append(out, prevSM)
			out = append(out, e)
			isPrevOut = true
			continue
		}
		if e.TripId != prevSM.TripId {
			prevSM = e
			isPrevOut = false
			continue
		}
	}
	return
}


// filterDeCancelled  some stop measures are repeated, once cancelled, then added, for whatever business reason
// in such cases, we keep only one and write them not cancelled or additional
// we make a key for identical stops, based on s.Station.Id, s.NextStationId, s.ArrivalTime, s.DepartureTime, s.TransportType
func filterDeCancelled(in []*ist.StopMeasure) []*ist.StopMeasure {
	stopSignature := func(s *ist.StopMeasure)string{
		nextStationId :=""
		if s.NextStopMeasure != nil{
			nextStationId = s.NextStopMeasure.Station.Id
		}
		return fmt.Sprintf("%v|%v|%v|%v|%v", s.Station.Id, nextStationId, s.ArrivalTime, s.DepartureTime, s.TransportType)
	}

	//group StopMeasure by their location/time
	bySignature := make(map[string][]*ist.StopMeasure)
	for _, sm := range (in) {
		signature := stopSignature(sm)
		_, ok:=bySignature[signature]
		if ! ok{
			bySignature[signature] = make([]*ist.StopMeasure, 0)
		}
		bySignature[signature] = append(bySignature[signature], sm)
	}

	out := make([]*ist.StopMeasure,0)
	nTot := 0
	nRemoved := 0
	for _, sms := range(bySignature){
		nTot = nTot + 1
		// 1 for signature, no conflict
		if len(sms) == 1 {
			out = append(out, sms[0])
			continue
		}

		// moe than 1 StopMeasure per signature
		// we must fin the one with the delay information and turn cancaellation/addiotinal flags off
		var smSelect *ist.StopMeasure
		for _, sm:= range(sms){
			//log.Println(fmt.Sprintf("\t%v", sm))
			if sm.DepartureActual!=nil || sm.ArrivalActual!=nil{
				smSelect = sm
			}
		}
		if smSelect== nil{
			smSelect=sms[0]
		}
		//log.Println(fmt.Sprintf("==>\t%v", smSelect))

		nRemoved = nRemoved + len(sms)-1
		smSelect.IsAdditionalDriving=false
		smSelect.IsCancelled = false
		out = append(out, smSelect)

	}
	log.Println(fmt.Sprintf("removed cancelled/dup stop masures: %v/%v", nRemoved, nTot))

	return out
}

// filterDeCancelledByTrips  some stop measures are repeated, once cancelled, then added, for whatever business reason
// in such cases, we keep only one and write them not cancelled or additional, bu t we focus at the trip level
func filterDeCancelledByTrips(in []*ist.StopMeasure) (out []*ist.StopMeasure, err error) {
	//group by tripId
	trips := make(map[string]*ist.TripWithStopMeasures)
	for _, sm := range (in) {
		tripId := sm.TripId
		twsm, ok := trips[tripId]
		if (ok) {
			twsm.Append(sm)
			continue
		}
		t:= ist.NewTripWithStopMeasures(tripId)
		trips[tripId] = &t
		trips[tripId].Append(sm)
	}

	//group trip by signature
	bySignature := make(map[string][]*ist.TripWithStopMeasures)
	for _, t := range (trips) {
		signature := t.Signature()
		_, ok := bySignature[signature]
		if (ok) {
			bySignature[signature] = append(bySignature[signature], t)
			continue
		}
		bySignature[signature] = []*ist.TripWithStopMeasures{t}
	}

	//pretty rough as we don't check for cancellation coherence
	// let's assume that if a trip is repeated, it is because one have been cancelled and another added
	out = make([]*ist.StopMeasure,0)
	nTot := 0
	nRemoved := 0
	for _, tripList := range (bySignature) {
		_, err := tripList[0].IsFullyCancelled()
		if  err != nil{
			return nil, err
		}
		//output as is
		if len(tripList) == 1{
			for _, sm:=range(tripList[0].StopsMeasures) {
				out = append(out, sm)
			}
			nTot += len (tripList[0].StopsMeasures)
			continue
		}
		for _, sm:=range(tripList[0].StopsMeasures) {
			sm.IsCancelled = false
			sm.IsAdditionalDriving = false
			out = append(out, sm)
		}
		nTot += len(tripList[0].StopsMeasures)
		nRemoved += (len(tripList)-1)*len(tripList[0].StopsMeasures)
		continue
	}
	log.Println(fmt.Sprintf("removed cancelled/dup %v/%v", nRemoved, nTot))
	return out, nil
}
