package gtfs

import "testing"

func Test_Trip_ReadFromCSVFile_count(t *testing.T) {
	routeReader := NewRouteReader()
	routeFile := "../data_test/routes.txt"
	dRoutes, err := routeReader.ReadFromCSVFile(routeFile)
	if err != nil {
		t.Error(err)
	}

	tripReader := NewTripReader()
	tripFile := "../data_test/trips.txt"
	dTrips, err := tripReader.ReadFromCSVFile(tripFile, dRoutes)
	if err != nil {
		t.Error(err)
	}

	n := dTrips.Size()
	want := 2408
	if n != want {
		t.Errorf("dictionary size differ got=%v want=%v", n, want)
	}
}
