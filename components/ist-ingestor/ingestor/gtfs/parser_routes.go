package gtfs

import (
	"octo.ch/swiss-transport-ist/ingestor/parser_commons"
	"octo.ch/swiss-transport-ist/schedule"
	"strconv"
)

type RouteReader struct {
	*parser_commons.Reader
}

func NewRouteReader() *RouteReader {
	return &RouteReader{
		&parser_commons.Reader{
			HeaderRefLine: "route_id,agency_id,route_short_name,route_long_name,route_desc,route_type",
			HeaderSep:     ',',
		},
	}
}

//ReadFromCSVFile parse the GTFS routes, only keeping those referring to a train
func (r *RouteReader) ReadFromCSVFile(filename string) (*schedule.RouteDict, error) {
	records, err := r.ReadRecordsFromFile(filename)
	if err != nil {
		return nil, err
	}

	dict := schedule.NewRouteDict()
	for _, rec := range records {
		t, err := strconv.Atoi(rec[5])
		if err != nil {
			return nil, err
		}
		route := schedule.Route{
			Id:          rec[0],
			Description: rec[4],
			Type:        t,
		}
		if route.IsTrain() {
			dict.Add(&route)
		}
	}
	return &dict, nil
}
