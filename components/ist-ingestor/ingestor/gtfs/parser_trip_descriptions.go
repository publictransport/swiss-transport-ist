package gtfs

import (
	"octo.ch/swiss-transport-ist/ingestor/parser_commons"
	"octo.ch/swiss-transport-ist/schedule"
)

type TripReader struct {
	*parser_commons.Reader
}

func NewTripReader() *TripReader {
	return &TripReader{
		&parser_commons.Reader{
			HeaderRefLine: "route_id,service_id,trip_id,trip_headsign,trip_short_name,direction_id",
			HeaderSep:     ',',
		},
	}
}

//ReadFromCSVFile read the trip descriptions, only keep those with a known route
// WARNING: for this project, we only gather one trip per route
func (r *TripReader) ReadFromCSVFile(filename string, routes *schedule.RouteDict) (*schedule.TripDescriptionDict, error) {

	records, err := r.ReadRecordsFromFile(filename)
	if err != nil {
		return nil, err
	}

	dict := schedule.TripDescriptionDict{}
	seenRoutes := make(map[string]bool)
	for _, rec := range records {
		_, ok:=seenRoutes[rec[0]]
		if ok{
			continue
		}
		seenRoutes[rec[0]]=true
		trip := schedule.TripDescription{
			Id:      rec[2],
			RouteId: rec[0],
		}
		if routes.Contains(trip.RouteId) {
			dict.Add(&trip)
		}
	}
	return &dict, nil
}
