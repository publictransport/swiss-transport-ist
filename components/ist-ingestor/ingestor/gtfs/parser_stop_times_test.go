package gtfs

import "testing"

func Test_StopTimes_ReadFromCSVFile_count(t *testing.T) {
	routeReader := NewRouteReader()
	routeFile := "../data_test/routes.txt"
	dRoutes, err := routeReader.ReadFromCSVFile(routeFile)
	if err != nil {
		t.Error(err)
	}

	tripReader := NewTripReader()
	tripFile := "../data_test/trips.txt"
	dTrips, err := tripReader.ReadFromCSVFile(tripFile, dRoutes)
	if err != nil {
		t.Error(err)
	}

	stopReader := NewStopReader()
	stopFile := "../data_test/stops.txt"
	dStops, err := stopReader.ReadFromCSVFile(stopFile)
	if err != nil {
		t.Error(err)
	}

	stopTimeReader := NewStopTimeReader()
	stopTimeFile := "../data_test/stop_times.txt"
	trips, err := stopTimeReader.ReadFromCSVFile(
		stopTimeFile,
		dTrips,
		dStops,
	)
	if err != nil {
		t.Error(err)
	}

	n := len(trips)
	want := 43
	if n != want {
		t.Errorf("trip list size differ got=%v want=%v", n, want)
	}
}
