package gtfs

import (
	"fmt"
	"log"
	"octo.ch/swiss-transport-ist/schedule"
)

func ReadStopsAndTripsFromCSVFile(dirname string) (*schedule.StopDict, []*schedule.Trip, error) {
	routeReader := NewRouteReader()
	routeFile := fmt.Sprintf("%v/routes.txt", dirname)
	dRoutes, err := routeReader.ReadFromCSVFile(routeFile)
	if err != nil {
		return nil, nil, err
	}
	log.Println(fmt.Sprintf("loaded %v routes", dRoutes.Size()))

	tripReader := NewTripReader()
	tripFile := fmt.Sprintf("%v/trips.txt", dirname)
	dTrips, err := tripReader.ReadFromCSVFile(tripFile, dRoutes)
	if err != nil {
		return nil, nil, err
	}
	log.Println(fmt.Sprintf("loaded %v trip descriptions", dTrips.Size()))

	stopReader := NewStopReader()
	stopFile := fmt.Sprintf("%v/stops.txt", dirname)
	dStops, err := stopReader.ReadFromCSVFile(stopFile)
	if err != nil {
		return nil, nil, err
	}
	log.Println(fmt.Sprintf("loaded %v stops", dStops.Size()))

	stopTimeReader := NewStopTimeReader()
	stopTimeFile := fmt.Sprintf("%v/stop_times.txt", dirname)
	trips, err := stopTimeReader.ReadFromCSVFile(
		stopTimeFile,
		dTrips,
		dStops,
	)
	if err != nil {
		return nil, nil, err
	}
	log.Println(fmt.Sprintf("loaded %v trips", len(trips)))
	return dStops, trips, nil
}
