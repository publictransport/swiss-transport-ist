package gtfs

import (
	"fmt"
	"log"
	"octo.ch/swiss-transport-ist/geo"
	"octo.ch/swiss-transport-ist/ingestor/parser_commons"
	"octo.ch/swiss-transport-ist/schedule"
	"regexp"
	"strconv"
)

type StopReader struct {
	*parser_commons.Reader
}

func NewStopReader() *StopReader {
	return &StopReader{
		&parser_commons.Reader{
			HeaderRefLine: "stop_id,stop_name,stop_lat,stop_lon,location_type,parent_station",
			HeaderSep:     ',',
		},
	}
}

// ReadFromCSVFile parse a CSV file and return a list of Stop
func (r *StopReader) ReadFromCSVFile(filename string) (*schedule.StopDict, error) {
	records, err := r.ReadRecordsFromFile(filename)
	if err != nil {
		return nil, err
	}

	dict := schedule.NewStopDict()
	for i, rec := range records {
		rec[0] = trimStopId(rec[0])
		st, err := r.newFromCSVLine(rec)
		if err != nil {
			log.Println(fmt.Sprintf("Error reading ist.Stop from file [%v] at line [%v]", filename, i+2))
			return nil, err
		}
		_, err = dict.Put(st)
		if err != nil {
			return nil, err
		}
	}

	return dict, nil
}

// newStopMeasureFromCSVLine a ist.StopMeasure constructor from a csv line in block (position do matter)
func (r *StopReader) newFromCSVLine(recs []string) (*schedule.Stop, error) {
	lat, err := strconv.ParseFloat(recs[2], 64)
	if err != nil {
		return nil, err
	}
	lon, err := strconv.ParseFloat(recs[3], 64)
	if err != nil {
		return nil, err
	}

	station := &schedule.Stop{
		Id:     recs[0],
		Name:   recs[1],
		Coords: geo.Coordinates{Lat: lat, Lon: lon},
	}
	return station, nil

}

//trimStopId in an order to unify station Id for the puprose of this project, we remove the trailing "P" or :0:1
//This is because we only need the station and not the voie
func trimStopId(id string) string {
	reIdP, _ := regexp.Compile("(?:P|:[:\\d]+)$")
	return reIdP.ReplaceAllString(id, "")
}

type StopNotFoundError struct {
	Id   string
	Name string
}

func (e StopNotFoundError) Error() string {
	return fmt.Sprintf("no station found for Id [%v] (%v)", e.Id, e.Name)
}
