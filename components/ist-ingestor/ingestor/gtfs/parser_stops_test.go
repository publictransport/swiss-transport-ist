package gtfs

import (
	"testing"
)

func Test_ReadStopsFromCSVFile_count(t *testing.T) {
	reader := NewStopReader()
	filename := "../data_test/stops.txt"
	dict, err := reader.ReadFromCSVFile(filename)
	if err != nil {
		t.Error(err)
	}
	n := dict.Size()
	want := 26329
	if n != want {
		t.Errorf("dictionary size differ got=%v want=%v", n, want)
	}
}

func Test_trimStopId(t *testing.T) {
	type args struct {
		id string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"straight", args{"123"}, "123"},
		{"ends with P", args{"123P"}, "123"},
		{"ends with track", args{"123:0:1"}, "123"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := trimStopId(tt.args.id); got != tt.want {
				t.Errorf("trimStopId() = %v, want %v", got, tt.want)
			}
		})
	}
}
