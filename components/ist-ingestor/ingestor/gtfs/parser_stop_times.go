package gtfs

import (
	"fmt"
	"log"
	"octo.ch/swiss-transport-ist/ingestor/parser_commons"
	"octo.ch/swiss-transport-ist/schedule"
	"sort"
	"strconv"
)

type stopTime struct {
	tripId       string
	stopId       string
	stopSequence int
}

type StopTimeReader struct {
	*parser_commons.Reader
}

func NewStopTimeReader() *StopTimeReader {
	return &StopTimeReader{
		&parser_commons.Reader{
			HeaderRefLine: "trip_id,arrival_time,departure_time,stop_id,stop_sequence,pickup_type,drop_off_type",
			HeaderSep:     ',',
		},
	}
}

// ReadFromCSVFile build a list of trips
func (r *StopTimeReader) ReadFromCSVFile(filename string, dTripDesc *schedule.TripDescriptionDict, dStop *schedule.StopDict) ([]*schedule.Trip, error) {
	records, err := r.ReadRecordsFromFile(filename)
	if err != nil {
		return nil, err
	}

	// first we pile stop times into a map by trip ID
	//once it full, we'll group them into a collection of Trip
	tripStopTimes := make(map[string][]stopTime)
	for _, rec := range records {
		if !dTripDesc.Contains(rec[0]){
			continue
		}
		seq, err := strconv.Atoi(rec[4])
		if err != nil {
			return nil, err
		}

		st := stopTime{
			rec[0],
			trimStopId(rec[3]),
			seq,
		}

		//skip if the trip is not registered
		if !dTripDesc.Contains(st.tripId) {
			continue
		}
		_, ok := tripStopTimes[st.tripId]
		if !ok {
			l := make([]stopTime, 0)
			tripStopTimes[st.tripId] = l
		}
		tripStopTimes[st.tripId] = append(tripStopTimes[st.tripId], st)

	}
	log.Println(fmt.Sprintf("nb records %v, nbTrips %v", len(records), len(tripStopTimes)))

	trips := make([]*schedule.Trip, 0)
	for tripId, sts := range tripStopTimes {
		sort.Slice(sts, func(i, j int) bool { return sts[i].stopSequence < sts[j].stopSequence })
		stops := make([]*schedule.Stop, 0)
		for _, s := range sts {
			stop, err := dStop.Get(s.stopId)
			if err != nil {
				return nil, err
			}
			stops = append(stops, stop)
		}
		t := schedule.NewTrip(tripId, stops)
		trips = append(trips, t)
	}
	return trips, nil
}
