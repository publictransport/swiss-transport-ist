package gtfs

import "testing"

func Test_Route_ReadFromCSVFile_count(t *testing.T) {
	reader := NewRouteReader()
	filename := "../data_test/routes.txt"
	dict, err := reader.ReadFromCSVFile(filename)
	if err != nil {
		t.Error(err)
	}
	n := dict.Size()
	want := 8
	if n != want {
		t.Errorf("dictionary size differ got=%v want=%v", n, want)
	}
}
