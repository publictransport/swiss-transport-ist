module octo.ch/swiss-transport-ist

require (
	github.com/go-pg/pg v6.15.1+incompatible
	github.com/go-test/deep v1.0.1
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	mellium.im/sasl v0.2.1 // indirect
)
