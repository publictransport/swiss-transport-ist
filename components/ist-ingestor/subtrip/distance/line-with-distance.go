package distance

import (
	"fmt"
	"math"
	"octo.ch/swiss-transport-ist/schedule"
)

type Linear struct {
	stations        []*schedule.Stop
	StationDistance map[string]float64
	totalDistance   float64
}

func (l *Linear) String() (s string) {
	s = ""
	for _, st := range l.stations {
		s += fmt.Sprintf("%v (%v km)\n", st.Name, math.Round(l.StationDistance[st.Id]/1000.0))
	}
	return s
}

func NewLinear(stations []*schedule.Stop) *Linear {
	td := make(map[string]float64)
	dTot := 0.0
	var prevStation *schedule.Stop
	for _, s := range stations {
		if prevStation != nil {
			// that's just but the first looping
			dTot += prevStation.Coords.Distance(s.Coords)
			//log.Println(fmt.Sprintf("%v\t%v\t%v\t%v\t%v\t%v\t%v", prevStation.Coords.Distance(s.Coords)/1000, dTot/1000, stations.Head().Coords.Distance(s.Coords)/1000, prevStation.Coords, s.Coords, prevStation.Name, s.Name))
		}
		prevStation = s
		td[s.Id] = dTot
	}

	return &Linear{
		stations,
		td,
		dTot,
	}
}

func (l *Linear) Contains(stopId string) bool {
	_, ok := l.StationDistance[stopId]
	return ok
}
