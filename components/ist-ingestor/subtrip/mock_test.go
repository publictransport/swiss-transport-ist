package subtrip

import (
	"log"
	"octo.ch/swiss-transport-ist/schedule"
)

func buildMockStationDict() (dict *schedule.StopDict) {
	dict = schedule.NewStopDict()
	dict.Put(&schedule.Stop{Id: "a", Name: "aaa"})
	dict.Put(&schedule.Stop{Id: "b", Name: "bbb"})
	dict.Put(&schedule.Stop{Id: "c", Name: "ccc"})
	dict.Put(&schedule.Stop{Id: "d", Name: "ddd"})
	dict.Put(&schedule.Stop{Id: "e", Name: "eee"})
	dict.Put(&schedule.Stop{Id: "f", Name: "fff"})
	dict.Put(&schedule.Stop{Id: "g", Name: "ggg"})
	dict.Put(&schedule.Stop{Id: "h", Name: "hhh"})
	return dict
}

var mockStations = buildMockStationDict()

func getMockStations(id string) *schedule.Stop {
	s, err := mockStations.Get(id)
	if err != nil {
		log.Panic(err)
	}
	return s
}
