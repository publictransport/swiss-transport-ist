package subtrip

import (
	"octo.ch/swiss-transport-ist/ist"
	"octo.ch/swiss-transport-ist/schedule"
	"sort"
)

// ListTrips build a set of Trip, referenced  by their Id, based on a list of ist.StopMeasure
func ListTrips(entries []*ist.StopMeasure) (trips []*schedule.Trip) {
	lowerStopMeasures := func(sms []*ist.StopMeasure) func(int, int) bool {
		return func(i, j int) bool {
			a := sms[i].DepartureTime
			b := sms[j].DepartureTime
			//format := func(t *time.Time) string {
			//	if t == nil {
			//		return "--:--"
			//	}
			//	return t.Format("15:04")
			//}
			if a == nil {
				//log.Println(fmt.Sprintf("cmp\t%v\t%v\t%v\t%v\t%v", format(a), format(b), i, j, false))
				return false
			}
			if b == nil {
				//log.Println(fmt.Sprintf("cmp\t%v\t%v\t%v\t%v\t%v", format(a), format(b), i, j, true))
				return true
			}
			//log.Println(fmt.Sprintf("cmp\t%v\t%v\t%v\t%v\t%v", format(a), format(b), i, j, a.Before(*b)))
			return a.Before(*b)
		}
	}
	//we shall group the entries by tripId, in oder to then sort them before creating the actual trips
	tripId2measures := make(map[string][]*ist.StopMeasure)
	for _, e := range entries {
		_, ok := tripId2measures[e.TripId]
		if !ok {
			tripId2measures[e.TripId] = make([]*ist.StopMeasure, 0)
		}
		tripId2measures[e.TripId] = append(tripId2measures[e.TripId], e)
	}

	//build the returned list
	trips = make([]*schedule.Trip, 0)
	for tripId, sms := range tripId2measures {
		//log.Println("-------------------------------")
		//for _, sm := range (sms) {
		//	log.Println(fmt.Sprintf("%v\t%v\t%v\t%v", tripId, sm.DepartureTime, sm.StationId, sm.Stop.Name))
		//}
		sort.Slice(sms, lowerStopMeasures(sms))
		stations := make([]*schedule.Stop, len(sms))
		for i, sm := range sms {
			//log.Println(fmt.Sprintf("%v\t%v\t%v\t%v", tripId, sm.DepartureTime, sm.StationId, sm.Stop.Name))
			stations[i] = sm.Station
		}
		trips = append(trips, schedule.NewTrip(tripId, stations))
	}
	sort.Slice(trips, func(i, j int) bool { return trips[i].Id < trips[j].Id })
	return
}
