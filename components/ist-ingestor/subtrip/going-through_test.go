package subtrip

import (
	"fmt"
	"octo.ch/swiss-transport-ist/schedule"
	"testing"

	"github.com/go-test/deep"
	"octo.ch/swiss-transport-ist/ist"
)

func buildMockStationsList(ids []string) (stations []*schedule.Stop) {
	stations = make([]*schedule.Stop, 0)
	for _, id := range ids {
		stations = append(stations, getMockStations(id))
	}
	return
}

func setFromString(xs []string) (set map[string]bool) {
	set = make(map[string]bool)
	for _, x := range xs {
		set[x] = true
	}
	return
}

func Test_subTripFrom(t *testing.T) {
	type args struct {
		sa   *schedule.Stop
		sb   *schedule.Stop
		trip *schedule.Trip
	}
	trip := schedule.NewTrip("1", buildMockStationsList([]string{"a", "b", "c", "d", "e", "f"}))
	tests := []struct {
		name string
		args args
		want *SubTrip
	}{
		{
			"same anchor mockStations",
			args{
				getMockStations("a"),
				getMockStations("a"),
				trip,
			},
			nil,
		},
		{
			"full",
			args{
				getMockStations("a"),
				getMockStations("f"),
				trip,
			},
			&SubTrip{
				schedule.NewTrip("1>[a:f]", buildMockStationsList([]string{"a", "b", "c", "d", "e", "f"})),
				trip,
			},
		},
		{
			"starts",
			args{
				getMockStations("a"),
				getMockStations("c"),
				trip,
			},
			&SubTrip{
				schedule.NewTrip("1>[a:c]", buildMockStationsList([]string{"a", "b", "c"})),
				trip,
			},
		},
		{
			"ends",
			args{
				getMockStations("d"),
				getMockStations("f"),
				trip,
			},
			&SubTrip{
				schedule.NewTrip("1>[d:f]", buildMockStationsList([]string{"d", "e", "f"})),
				trip,
			},
		},
		{
			"middle",
			args{
				getMockStations("b"),
				getMockStations("d"),
				trip,
			},
			&SubTrip{
				schedule.NewTrip("1>[b:d]", buildMockStationsList([]string{"b", "c", "d"})),
				trip,
			},
		},
		{
			"middle adjacent",
			args{
				getMockStations("b"),
				getMockStations("c"),
				trip,
			},
			&SubTrip{
				schedule.NewTrip("1>[b:c]", buildMockStationsList([]string{"b", "c"})),
				trip,
			},
		}, {
			"full reverse",
			args{
				getMockStations("f"),
				getMockStations("a"),
				trip,
			},
			&SubTrip{
				schedule.NewTrip("1>[a:f]", buildMockStationsList([]string{"a", "b", "c", "d", "e", "f"})),
				trip,
			},
		},
		{
			"starts reverse",
			args{
				getMockStations("c"),
				getMockStations("a"),
				trip,
			},
			&SubTrip{
				schedule.NewTrip("1>[a:c]", buildMockStationsList([]string{"a", "b", "c"})),
				trip,
			},
		},
		{
			"ends reverse",
			args{
				getMockStations("f"),
				getMockStations("d"),
				trip,
			},
			&SubTrip{
				schedule.NewTrip("1>[d:f]", buildMockStationsList([]string{"d", "e", "f"})),
				trip,
			},
		},
		{
			"middle reverse",
			args{
				getMockStations("d"),
				getMockStations("b"),
				trip,
			},
			&SubTrip{
				schedule.NewTrip("1>[b:d]", buildMockStationsList([]string{"b", "c", "d"})),
				trip,
			},
		},
		{
			"middle adjacent reverse",
			args{
				getMockStations("c"),
				getMockStations("b"),
				trip,
			},
			&SubTrip{
				schedule.NewTrip("1>[b:c]", buildMockStationsList([]string{"b", "c"})),
				trip,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := subTripFrom(tt.args.sa, tt.args.sb, tt.args.trip)
			diff := deep.Equal(got, tt.want)
			if diff != nil {
				t.Errorf("subTripFrom() = %T, want %T", got, tt.want)
				t.Errorf(`subTripFrom() =
got:  %v
want  %v`, got, tt.want)
				t.Error(diff)
			}
		})
	}
}

type subTripExtract struct {
	parentId string
	intermId string
	stations []*schedule.Stop
}

func extractSubTrip(trips []*SubTrip) (sle []subTripExtract) {
	sle = make([]subTripExtract, 0)
	for _, l := range trips {
		sn := make([]*schedule.Stop, 0)
		for _, n := range l.Stops {
			sn = append(sn, n)
		}
		sle = append(sle, subTripExtract{l.Id, l.Id, sn})
	}
	return
}

func Test_goingThroughOneAnchor(t *testing.T) {

	entries := []*ist.StopMeasure{
		&ist.StopMeasure{Station: getMockStations("a"), TripId: "1"},
		&ist.StopMeasure{Station: getMockStations("b"), TripId: "1"},
		&ist.StopMeasure{Station: getMockStations("c"), TripId: "1"},
		&ist.StopMeasure{Station: getMockStations("d"), TripId: "1"},
		&ist.StopMeasure{Station: getMockStations("e"), TripId: "1"},
		&ist.StopMeasure{Station: getMockStations("b"), TripId: "2"},
		&ist.StopMeasure{Station: getMockStations("d"), TripId: "2"},
		&ist.StopMeasure{Station: getMockStations("h"), TripId: "2"},
		&ist.StopMeasure{Station: getMockStations("f"), TripId: "3"},
		&ist.StopMeasure{Station: getMockStations("g"), TripId: "3"},
	}
	trips := ListTrips(entries)

	type args struct {
		a     schedule.Bound
		trips []*schedule.Trip
	}
	tests := []struct {
		name           string
		args           args
		wantSubtripIds []subTripExtract
	}{
		{
			name: "one",
			args: args{a: schedule.Bound{getMockStations("f"), getMockStations("g")}, trips: trips},
			wantSubtripIds: []subTripExtract{
				{
					parentId: "3",
					intermId: "3:f/g",
					stations: buildMockStationsList([]string{"f", "g"}),
				},
			},
		},
		{
			name: "multiples",
			args: args{a: schedule.Bound{getMockStations("b"), getMockStations("d")}, trips: trips},
			wantSubtripIds: []subTripExtract{
				{
					parentId: "1",
					intermId: "1:b/d",
					stations: buildMockStationsList([]string{"b", "c", "d"}),
				},
				{
					parentId: "2",
					intermId: "2:b/d",
					stations: buildMockStationsList([]string{"b", "d"}),
				},
			},
		},
	}
	for _, tt := range tests {
		gotSubtrips := extractSubTrip(subtripsGoingThroughOneAnchor(tt.args.a, tt.args.trips))

		t.Run(tt.name, func(t *testing.T) {
			diff := deep.Equal(gotSubtrips, tt.wantSubtripIds)
			if diff != nil {
				t.Errorf("subTripFrom() = %T, want %T", gotSubtrips, tt.wantSubtripIds)
				t.Errorf(`subtripsGoingThroughOneAnchor()
got:  %v
want: %v`, gotSubtrips, tt.wantSubtripIds)
				t.Error(diff)
			}
		})
	}
}

func TestThroughTripIndex_IsStopMeasureIn(t *testing.T) {

	entries := []*ist.StopMeasure{
		&ist.StopMeasure{Station: getMockStations("a"), TripId: "1"},
		&ist.StopMeasure{Station: getMockStations("b"), TripId: "1"},
		&ist.StopMeasure{Station: getMockStations("c"), TripId: "1"},
		&ist.StopMeasure{Station: getMockStations("d"), TripId: "1"},
		&ist.StopMeasure{Station: getMockStations("e"), TripId: "1"},
		&ist.StopMeasure{Station: getMockStations("b"), TripId: "2"},
		&ist.StopMeasure{Station: getMockStations("d"), TripId: "2"},
		&ist.StopMeasure{Station: getMockStations("h"), TripId: "2"},
		&ist.StopMeasure{Station: getMockStations("f"), TripId: "3"},
		&ist.StopMeasure{Station: getMockStations("g"), TripId: "3"},
	}
	trips := ListTrips(entries)
	bounds := map[string]schedule.Bound{
		"forwards":  schedule.Bound{getMockStations("b"), getMockStations("d")},
		"backwards": schedule.Bound{getMockStations("d"), getMockStations("b")},
	}

	type args struct {
		TripId    string
		stationId string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "in start",
			args: args{TripId: "1", stationId: "b"},
			want: true,
		},
		{
			name: "in middle",
			args: args{TripId: "1", stationId: "c"},
			want: true,
		},
		{
			name: "in end",
			args: args{TripId: "1", stationId: "b"},
			want: true,
		},
		{
			name: "out before",
			args: args{TripId: "1", stationId: "a"},
			want: false,
		},
		{
			name: "out after",
			args: args{TripId: "1", stationId: "e"},
			want: false,
		},
	}
	for dir, bound := range bounds {
		for _, tt := range tests {
			t.Run(fmt.Sprintf("%v/%v", dir, tt.name), func(t *testing.T) {
				tli := NewThroughTripIndex(bound)

				for _, sl := range subtripsGoingThroughOneAnchor(bound, trips) {
					err := tli.AddSubTrip(sl)
					if err != nil {
						t.Error(err)
					}
				}

				sm := &ist.StopMeasure{TripId: tt.args.TripId, Station: getMockStations(tt.args.stationId)}
				if got := tli.IsStopMeasureIn(sm); got != tt.want {
					t.Errorf("ThroughTripIndex.IsStopMeasureIn() = %v, want %v", got, tt.want)
				}
			})
		}
	}
}

func TestThroughTripIndex_GreatestCommonDenominator(t *testing.T) {

	entries := []*ist.StopMeasure{
		&ist.StopMeasure{Station: getMockStations("a"), TripId: "1"},
		&ist.StopMeasure{Station: getMockStations("b"), TripId: "1"},
		&ist.StopMeasure{Station: getMockStations("d"), TripId: "1"},
		&ist.StopMeasure{Station: getMockStations("e"), TripId: "1"},
		&ist.StopMeasure{Station: getMockStations("f"), TripId: "1"},
		&ist.StopMeasure{Station: getMockStations("g"), TripId: "1"},
		&ist.StopMeasure{Station: getMockStations("a"), TripId: "2"},
		&ist.StopMeasure{Station: getMockStations("b"), TripId: "2"},
		&ist.StopMeasure{Station: getMockStations("c"), TripId: "2"},
		&ist.StopMeasure{Station: getMockStations("d"), TripId: "2"},
		&ist.StopMeasure{Station: getMockStations("g"), TripId: "2"},
	}
	trips := ListTrips(entries)
	bound := schedule.Bound{getMockStations("a"), getMockStations("g")}
	tli := NewThroughTripIndex(bound)
	for _, sl := range subtripsGoingThroughOneAnchor(bound, trips) {
		tli.AddSubTrip(sl)
	}
	gcd, err := tli.GreatestCommonDenominator()
	if err != nil {
		t.Errorf("error returned %v", err)
	} else {
		got := make([]string, 0)
		for _, x := range gcd {
			got = append(got, x.Id)
		}
		want := []string{"a", "b", "c", "d", "e", "f", "g"}
		diff := deep.Equal(got, want)
		if diff != nil {
			t.Errorf("GreatestCommonDenominator got=%v want=%v", got, want)
			t.Error(diff)
		}
	}
}
