package subtrip

import (
	"fmt"
	"octo.ch/swiss-transport-ist/schedule"
)

type SubTrip struct {
	*schedule.Trip
	Parent *schedule.Trip
}

// the list of trip that goes though two mockStations
type AnchoredTrips struct {
	Anchor schedule.Bound
	trips  map[string]schedule.Trip
}

func (l *SubTrip) String() string {
	return fmt.Sprintf("%v // parent => %v", l.Trip, l.Parent)
}
