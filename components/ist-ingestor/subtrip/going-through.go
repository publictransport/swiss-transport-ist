package subtrip

import (
	"errors"
	"fmt"
	"log"
	"octo.ch/swiss-transport-ist/ist"
	"octo.ch/swiss-transport-ist/schedule"
)

// ThroughTripIndex given an Bound (i.e. two stations), holds all the SubTrip (the potion of wider Trip) that goes through
type ThroughTripIndex struct {
	Bound          schedule.Bound
	tripId2subtrip map[string]*SubTrip
}

func (tli *ThroughTripIndex) String() string {
	return fmt.Sprintf("%v (%v subtrips)", tli.Bound, tli.Size())
}

func NewThroughTripIndex(a schedule.Bound) *ThroughTripIndex {
	return &ThroughTripIndex{
		Bound:          a,
		tripId2subtrip: make(map[string]*SubTrip),
	}
}

// gGoingThrough extract the list of Trip that go through the two mockStations from the given Bound
func NewThroughTripIndexFromGoingBetween(a schedule.Bound, trips []*schedule.Trip) (ind *ThroughTripIndex, err error) {
	subtrips := subtripsGoingThroughOneAnchor(a, trips)
	ind = NewThroughTripIndex(a)
	for _, sl := range subtrips {
		err = ind.AddSubTrip(sl)
		if err != nil {
			log.Println(fmt.Sprintf("error adding subtrip %v: %v", sl, err))
			//return nil, err
		}
	}
	return
}

func (ind *ThroughTripIndex) Size() int {
	return len(ind.tripId2subtrip)
}

//AddSubTrip add a new subtrip
//Error are return if
//  * IncompatibleBoundError: the subtrip bound are not the same as the index one
//  * one parent trip has multiple different subtrips (I don't foesee how that could be possible...)
func (ind *ThroughTripIndex) AddSubTrip(sl *SubTrip) error {
	slb, _ := sl.GetBounds()
	if slb != ind.Bound && slb != ind.Bound.Reverse() {
		return &IncompatibleBoundError{ind.Bound, slb}
	}

	parentTripId := sl.Parent.Id
	slOk, ok := ind.tripId2subtrip[parentTripId]
	if ok && slOk.Id != sl.Id {
		return errors.New(fmt.Sprintf("the same parent trip [%v] has two different subtrips [%v]/[%v]", parentTripId, slOk.Id, sl.Id))
	}
	if ok {
		return nil
	}
	ind.tripId2subtrip[parentTripId] = sl
	return nil
}

func (ind *ThroughTripIndex) IsStopMeasureIn(sm *ist.StopMeasure) bool {
	_, ok := ind.tripId2subtrip[sm.TripId]
	if !ok {
		return false
	}
	return ind.tripId2subtrip[sm.TripId].IsPassingThrough(sm.Station)
}


//MostStoppedTrip returns the trip with the most stops with the index.
func  (ind *ThroughTripIndex) MostStoppedTrip() (*schedule.Trip, error) {
	if ind.Size() == 0 {
		return nil, errors.New("MostStoppedTrip: empty list of trips")
	}

	var trip *schedule.Trip
	var nmax= -1
	for _, t := range ind.tripId2subtrip {
		n := t.Size()
		if n > nmax {
			trip = t.Trip
			nmax = n
		}
	}
	return trip, nil
}

// GreatestCommonDenominator is the longest trip covering all stops grouped within
// for the moment, we just take the longest trip, betting it will cover all stops
// Error can be returned
// * EmptyThroughTripIndexError if the index is empty
// * StationNotCoveredByGreatesCommonDenominatorError after a post check, if some station from the subtrips is not covered by the GCD
func (ind *ThroughTripIndex) GreatestCommonDenominator() (gcdStations []*schedule.Stop, err error) {
	if ind.Size() == 0 {
		return nil, &EmptyThroughTripIndexError{ind}
	}
	lMax := 0
	for _, sl := range ind.tripId2subtrip {
		l := len(sl.Trip.Stops)

		//if the current trip is the longest, we keep it
		if l > lMax {
			lMax = l
			gcdStations = make([]*schedule.Stop, lMax)
			copy(gcdStations, sl.Trip.Stops)
			continue
		}

	}

	//let's now check out that all station are covered by this longest stationList
	stationSet := make(map[string]bool)
	for _, s := range gcdStations {
		stationSet[s.Id] = true
	}
	indexStation := func(x *schedule.Stop, xs []*schedule.Stop) int {
		for i, xi := range xs {
			if xi.Id == x.Id {
				return i
			}
		}
		return -1
	}
	for _, sl := range ind.tripId2subtrip {
		for is, s := range sl.Stops {
			_, ok := stationSet[s.Id]
			if !ok {
				// s is not found into the curren tlongest path.
				// as it cannot be the head/tail for the trip, let's find the two statin surrounding it
				prevStation := sl.Stops[is-1]
				nextStation := sl.Stops[is+1]
				iRefPrev := indexStation(prevStation, gcdStations)
				iRefNext := indexStation(nextStation, gcdStations)
				if iRefNext != iRefPrev+1 {
					log.Println(fmt.Sprintf("refPrev=%v, refNext=%v", iRefPrev, iRefNext))
					log.Println(fmt.Sprintf("%v", &StationNotCoveredByGreatesCommonDenominatorError{ind, gcdStations, s, sl}))

					//return nil, &StationNotCoveredByGreatesCommonDenominatorError{ind, gcdStations, s, sl}
					continue
				}
				// and we build a new gcd by inserting the renegade station where it lies
				gcdNew := make([]*schedule.Stop, len(gcdStations)+1)
				copy(gcdNew, gcdStations[:iRefPrev+1])
				gcdNew[iRefPrev+1] = s
				for i := iRefNext; i < len(gcdStations); i++ {
					gcdNew[i+1] = gcdStations[i]
				}
				gcdStations = gcdNew
			}
		}
	}
	// reverse the list if the first station is the tail for the index
	if *gcdStations[0] == *ind.Bound.Stop2 {
		n := len(gcdStations)
		for i := 0; i < n/2; i++ {
			x := gcdStations[i]
			gcdStations[i] = gcdStations[n-1-i]
			gcdStations[n-1-i] = x
		}
	}
	return
}

// subTripFrom extract the SubTrip for the given Trip, between the two mockStations
// if the trip does not go through both mockStations, or the two given mockStations are the same, nil is returned
func subTripFrom(sa, sb *schedule.Stop, l *schedule.Trip) *SubTrip {
	if !l.IsPassingThrough(sa) || !l.IsPassingThrough(sb) {
		return nil
	}
	if sa == sb {
		return nil
	}

	interm := make([]*schedule.Stop, 0)
	isIn := false
	isForwards := true
	for _, s := range l.Stops {
		if !isIn && (s == sa || s == sb) {
			//if we hit first sb, that means the the trip is running through the two mockStations, but in reverse order
			isForwards = s == sa

			isIn = true
			interm = append(interm, s)
			continue
		}
		//we are within the station
		if isIn {
			interm = append(interm, s)
		}
		//the second station is reached
		if isIn && (s == sa || s == sb) {
			isIn = false
		}
	}

	//reverse the order start/end mockStations for the intermediary trip  id
	asa := sa
	asb := sb
	if !isForwards {
		asa = sb
		asb = sa
	}

	return &SubTrip{
		schedule.NewTrip(fmt.Sprintf("%v>[%v:%v]", l.Id, asa.Id, asb.Id), interm),
		l,
	}
}

// gGoingThrough extract the list of Trip that go through the two mockStations from the given Bound
func subtripsGoingThroughOneAnchor(a schedule.Bound, trips []*schedule.Trip) (subtrips []*SubTrip) {
	subtrips = make([]*SubTrip, 0)
	for _, l := range trips {
		if l.IsPassingThrough(a.Stop1) && l.IsPassingThrough(a.Stop2) {
			subtrips = append(subtrips, subTripFrom(a.Stop1, a.Stop2, l))
		}
	}
	return
}

type IncompatibleBoundError struct {
	ref schedule.Bound
	add schedule.Bound
}

func (e *IncompatibleBoundError) Error() string {
	return fmt.Sprintf("IncompatibleBoundError ref=%v, adding%v", e.ref, e.add)
}

type EmptyThroughTripIndexError struct {
	index *ThroughTripIndex
}

func (e *EmptyThroughTripIndexError) Error() string {
	return fmt.Sprintf("EmptyThroughTripIndexError %v", e.index)
}

type StationNotCoveredByGreatesCommonDenominatorError struct {
	index       *ThroughTripIndex
	gcd         []*schedule.Stop
	station     *schedule.Stop
	stationTrip *SubTrip
}

func (e *StationNotCoveredByGreatesCommonDenominatorError) Error() string {
	return fmt.Sprintf(`StationNotCoveredByGreatesCommonDenominatorError %v (trip id %v) index %v
in trip  %v 
into gcd %v`, e.station, e.stationTrip.Id, e.index, e.stationTrip.Stops, e.gcd)
}
