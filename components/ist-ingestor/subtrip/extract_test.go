package subtrip

import (
	"github.com/go-test/deep"
	"log"
	"octo.ch/swiss-transport-ist/ist"
	"octo.ch/swiss-transport-ist/schedule"
	"testing"
	"time"
)

func parseDateHM(s string) *time.Time {
	t, err := time.Parse("02.01.2006 15:04", s)
	if err != nil {
		log.Panic(err)
	}
	return &t
}

func TestListTrips(t *testing.T) {
	type args struct {
		entries []*ist.StopMeasure
	}

	tests := []struct {
		name      string
		args      args
		wantTrips []*schedule.Trip
	}{
		{
			name: "empty",
			args: args{
				[]*ist.StopMeasure{},
			},
			wantTrips: []*schedule.Trip{},
		},
		{
			name: "one trip",
			args: args{
				[]*ist.StopMeasure{
					&ist.StopMeasure{Station: getMockStations("a"), TripId: "1", DepartureTime: parseDateHM("24.09.2018 05:00")},
					&ist.StopMeasure{Station: getMockStations("b"), TripId: "1", DepartureTime: parseDateHM("24.09.2018 05:01")},
					&ist.StopMeasure{Station: getMockStations("c"), TripId: "1", DepartureTime: parseDateHM("24.09.2018 05:02")},
				},
			},
			wantTrips: []*schedule.Trip{
				&schedule.Trip{
					Id: "1",
					StopIdSet: map[string]bool{
						"a": true,
						"b": true,
						"c": true,
					},
					Stops: []*schedule.Stop{getMockStations("a"), getMockStations("b"), getMockStations("c")},
				},
			},
		},
		{
			name: "multiples trips",
			args: args{
				[]*ist.StopMeasure{
					&ist.StopMeasure{Station: getMockStations("a"), TripId: "1", DepartureTime: parseDateHM("24.09.2018 05:00")},
					&ist.StopMeasure{Station: getMockStations("b"), TripId: "1", DepartureTime: parseDateHM("24.09.2018 05:01")},
					&ist.StopMeasure{Station: getMockStations("f"), TripId: "3", DepartureTime: parseDateHM("24.09.2018 06:00")},
					&ist.StopMeasure{Station: getMockStations("g"), TripId: "3", DepartureTime: nil},
					&ist.StopMeasure{Station: getMockStations("c"), TripId: "2", DepartureTime: parseDateHM("24.09.2018 07:00")},
					&ist.StopMeasure{Station: getMockStations("d"), TripId: "1", DepartureTime: parseDateHM("24.09.2018 05:02")},
					&ist.StopMeasure{Station: getMockStations("e"), TripId: "2", DepartureTime: nil},
					&ist.StopMeasure{Station: getMockStations("h"), TripId: "1", DepartureTime: nil},
				},
			},
			wantTrips: []*schedule.Trip{
				&schedule.Trip{
					Id: "1",
					StopIdSet: map[string]bool{
						"a": true,
						"b": true,
						"d": true,
						"h": true,
					},
					Stops: []*schedule.Stop{getMockStations("a"), getMockStations("b"), getMockStations("d"), getMockStations("h")},
				},
				&schedule.Trip{
					Id: "2",
					StopIdSet: map[string]bool{
						"c": true,
						"e": true,
					},
					Stops: []*schedule.Stop{getMockStations("c"), getMockStations("e")},
				},
				&schedule.Trip{
					Id: "3",
					StopIdSet: map[string]bool{
						"f": true,
						"g": true,
					},
					Stops: []*schedule.Stop{getMockStations("f"), getMockStations("g")},
				},
			},
		},
		{
			name: "input miss ordered",
			args: args{
				[]*ist.StopMeasure{
					&ist.StopMeasure{Station: getMockStations("a"), TripId: "1", DepartureTime: parseDateHM("24.09.2018 05:00")},
					&ist.StopMeasure{Station: getMockStations("h"), TripId: "1", DepartureTime: nil},
					&ist.StopMeasure{Station: getMockStations("d"), TripId: "1", DepartureTime: parseDateHM("24.09.2018 05:02")},
					&ist.StopMeasure{Station: getMockStations("b"), TripId: "1", DepartureTime: parseDateHM("24.09.2018 05:01")},
					&ist.StopMeasure{Station: getMockStations("c"), TripId: "2", DepartureTime: parseDateHM("24.09.2018 07:00")},
					&ist.StopMeasure{Station: getMockStations("e"), TripId: "2", DepartureTime: nil},
				},
			},
			wantTrips: []*schedule.Trip{
				&schedule.Trip{
					Id: "1",
					StopIdSet: map[string]bool{
						"a": true,
						"b": true,
						"d": true,
						"h": true,
					},
					Stops: []*schedule.Stop{getMockStations("a"), getMockStations("b"), getMockStations("d"), getMockStations("h")},
				},
				&schedule.Trip{
					Id: "2",
					StopIdSet: map[string]bool{
						"c": true,
						"e": true,
					},
					Stops: []*schedule.Stop{getMockStations("c"), getMockStations("e")},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotTrips := ListTrips(tt.args.entries)
			diff := deep.Equal(gotTrips, tt.wantTrips)
			if diff != nil {
				t.Errorf(`ListTrips() = 
got:  %v
want: %v`, gotTrips, tt.wantTrips)
				t.Errorf(`ListTrips() = 
got:  %v
want: %v`, gotTrips[0].StopIdSet, tt.wantTrips[0].StopIdSet)
				t.Error(diff)
			}
		})
	}
}
