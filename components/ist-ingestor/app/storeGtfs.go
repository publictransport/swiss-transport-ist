package main

import (
	"fmt"
	"log"
	"octo.ch/swiss-transport-ist/ingestor/gtfs"
	"octo.ch/swiss-transport-ist/store"
)



// insert the GTFS stops into the database
func main() {

	connector := store.Connector{}
	err := connector.Open()
	if err != nil {
		log.Panic(fmt.Sprintf("ERROR while opening the DBH connection: %v", err))
	}

	gtfsDirname := "data"
	stopDict, _, err := gtfs.ReadStopsAndTripsFromCSVFile(gtfsDirname)
	if err != nil {
		log.Panic(err)
	}


	l:=stopDict.List()
	err = connector.DB.Insert(&l)
	if err != nil {
		log.Panic(fmt.Sprintf("inserting stops in  DB: ", err))
	}

	log.Println(fmt.Sprintf("update %v stops in the database", stopDict.Size()))
}
