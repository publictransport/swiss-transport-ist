package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"octo.ch/swiss-transport-ist/ingestor/gtfs"
	"octo.ch/swiss-transport-ist/ingestor/parser_ist"
	"octo.ch/swiss-transport-ist/store"
	"strings"
	"sync/atomic"
)

func readAndInsert(connector *store.Connector, filename string, reader *parser_ist.StopMeasureReader) (int, error) {
	sms, err := reader.ReadStopMeasuresFromCSVFile(filename)
	if err != nil {
		log.Panic(err)
	}
	log.Println(fmt.Sprintf("read %v stops measures", len(sms)))

	connector.AttributeStopMeasureIds(sms)
	n, err := connector.InsertStopMeasures(sms)
	if err != nil {
		return 0, err
	}
	return n, nil
}

func main() {
	connector := store.NewConnector()
	err := connector.Open()
	if err != nil {
		log.Panic("ERROR while opening the DBH connection", err)
	}

	gtfsDirname := "data"
	stopDict, _, err := gtfs.ReadStopsAndTripsFromCSVFile(gtfsDirname)
	if err != nil {
		log.Panic(err)
	}

	//cvsFilename := "/Users/alex/tmp/ist-10k.csv"
	//cvsFilename := "/Users/alex/dev/bigdata/cff-data/realtime-dumps/data/translated/2018-01-09istdaten-en.csv"
	dirname := "/Users/alex/dev/bigdata/cff-data/realtime-dumps/data/translated"
	reader := parser_ist.NewStopMeasureReader(stopDict)

	filenames, err := ioutil.ReadDir(dirname)
	if err != nil {
		log.Panic(err)
	}
	var countFiles int64
	var total int64

	for _, fn := range filenames {
		if !strings.HasSuffix(fn.Name(), ".csv") {
			continue
		}
		//if fn.Name() != "a.csv" {
		//if fn.Name() != "2018-12-14istdaten-en.csv" {
		//	continue
		//}

		fullname := dirname + "/" + fn.Name()
		log.Println(fmt.Sprintf("Parsing: %v", fullname))

		n, err := readAndInsert(&connector, fullname, reader)
		if err != nil {
			log.Panic(fmt.Sprintf("error with [%v]\n%v", fullname, err))
		}
		atomic.AddInt64(&total, int64(n))
		atomic.AddInt64(&countFiles, int64(1))
	}


	fmt.Printf("processed %v stop measures in %v file\n", total, countFiles)
}
