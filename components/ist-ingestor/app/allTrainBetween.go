package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"octo.ch/swiss-transport-ist/ingestor/gtfs"
	"octo.ch/swiss-transport-ist/ingestor/parser_ist"
	"octo.ch/swiss-transport-ist/schedule"
	"octo.ch/swiss-transport-ist/subtrip"
	"octo.ch/swiss-transport-ist/subtrip/distance"
	"os"
	"strconv"
	"strings"
	"time"
)

func formatTime(t *time.Time) string {
	if t == nil {
		return ""
	}
	return t.Format(time.RFC3339)
}

func deltaTime(tPlan, tAct *time.Time) string {
	if tPlan == nil || tAct == nil {
		return ""
	}
	return strconv.FormatFloat(tAct.Sub(*tPlan).Seconds(), 'f', 0, 64)
}

func main() {

	outFilename := "/Users/alex/tmp/cff-delays-lausanne-geneve.tsv"
	out, err := os.Create(outFilename)
	if err != nil {
		log.Panic(err)
	}
	defer out.Close()
	out.WriteString("day\ttrip_id\ttransport_type\tis_additional_driving\tis_cancelled\tts_arrival_time\tts_arrival_actual\tarrival_delay\tts_departure_time\tts_departure_actual\tdeparture_delay\tstation_id\tstation_name\ttrip_distance\n")

	gtfsDirname := "data"
	stopDict, scheduleTrips, err := gtfs.ReadStopsAndTripsFromCSVFile(gtfsDirname)
	if err != nil {
		log.Panic(err)
	}

	//cout := make(chan string)
	station2, err := stopDict.Get("8501120") //"Lausanne")
	if err != nil {
		log.Panic(err)
	}
	station1, err := stopDict.Get("8501008") //Genève")
	if err != nil {
		log.Panic(err)
	}
	log.Println(fmt.Sprintf("all stops between %v and %v", station1, station2))

	anchor := schedule.Bound{station1, station2}

	tlScheduleIndex, err := subtrip.NewThroughTripIndexFromGoingBetween(anchor, scheduleTrips)
	longestTrip,err := tlScheduleIndex.MostStoppedTrip()
	if err != nil {
		log.Panic(err)
	}
	linear := distance.NewLinear(longestTrip.Stops)
	log.Println(linear)


	dirname := "/Users/alex/dev/bigdata/cff-data/realtime-dumps/data/translated"
	//dirname := "/tmp"
	reader := parser_ist.NewStopMeasureReader(stopDict)

	filenames, err := ioutil.ReadDir(dirname)
	if err != nil {
		log.Panic(err)
	}

	for _, fn := range filenames {
		if !strings.HasSuffix(fn.Name(), ".csv") {
			continue
		}
		//if fn.Name() != "a.csv" {
		//if fn.Name() != "2018-12-14istdaten-en.csv" {
		//	continue
		//}

		fullname := dirname + "/" + fn.Name()
		log.Println(fmt.Sprintf("Parsing: %v", fullname))
		stopMeasures, err := reader.ReadStopMeasuresFromCSVFile(fullname)
		if err != nil {
			log.Panic(err)
		}
		log.Println(fmt.Sprintf("processed %v measures", len(stopMeasures)))

		trips := subtrip.ListTrips(stopMeasures)
		tlIndex, err := subtrip.NewThroughTripIndexFromGoingBetween(anchor, trips)
		if err != nil {
			log.Panic(err)
		}
		log.Println(fmt.Sprintf("processed %v trips", len(trips)))
		log.Println(fmt.Sprintf("%v going through %v", tlIndex.Size(), tlIndex.Bound))

		for _, sm := range stopMeasures {
			if tlIndex.IsStopMeasureIn(sm)  && linear.Contains(sm.Station.Id){
				out.WriteString(
					fmt.Sprintf("%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\t%0.1f\n",
						sm.OperatingDay.Format("02/01/2006"),
						sm.TripId,
						sm.TransportType,
						sm.IsAdditionalDriving,
						sm.IsCancelled,
						formatTime(sm.ArrivalTime),
						formatTime(sm.ArrivalActual),
						deltaTime(sm.ArrivalTime, sm.ArrivalActual),
						formatTime(sm.DepartureTime),
						formatTime(sm.DepartureActual),
						deltaTime(sm.DepartureTime, sm.DepartureActual),
						sm.Station.Id,
						sm.Station.Name,
						linear.StationDistance[sm.Station.Id],
					))
			}
		}
	}

}
