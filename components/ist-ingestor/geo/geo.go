package geo

import (
	"fmt"
	"math"
)

type Coordinates struct {
	Lat float64
	Lon float64
}

func (c *Coordinates) String() string {
	return fmt.Sprintf("(%v, %v)", c.Lat, c.Lon)
}

func (c *Coordinates) Distance(c2 Coordinates) float64 {
	radius := 6371000.0
	dLat := deg2rad(c.Lat - c2.Lat)
	dLon := deg2rad(c.Lon - c2.Lon)
	x := math.Sin(dLat/2)*math.Sin(dLat/2) +
		math.Cos(deg2rad(c.Lat))*math.Cos(deg2rad(c2.Lat))*
			math.Sin(dLon/2)*math.Sin(dLon/2)
	y := 2 * math.Atan2(math.Sqrt(x), math.Sqrt(1-x))
	return radius * y

}

func deg2rad(d float64) float64 {
	return math.Pi / 180 * d
}

/*
https://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1);
  var a =
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ;
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}
*/
