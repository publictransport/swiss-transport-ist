package geo

import (
	"fmt"
	"math"
	"testing"
)

func TestCoordinates_distance(t *testing.T) {
	type fields struct {
		Lat float64
		Lon float64
	}
	type args struct {
		c2 Coordinates
	}
	tests := []struct {
		lat1, lon1, lat2, lon2 float64
		want, tolerance        float64
	}{
		{46.5169661916384, 6.62909069062426, 46.2105161699275, 6.14245533484329, 50551, 1},
	}
	for _, tt := range tests {
		c := Coordinates{
			Lat: tt.lat1,
			Lon: tt.lon1,
		}
		c2 := Coordinates{
			Lat: tt.lat2,
			Lon: tt.lon2,
		}
		t.Run(fmt.Sprintf("Distance %v-%v", c, c2), func(t *testing.T) {
			dist := c.Distance(c2)
			if math.Abs(dist-tt.want) > tt.tolerance {
				t.Errorf("Coordinates.Distance() = %v, want %v", dist, tt.want)
			}
		})
	}
}
