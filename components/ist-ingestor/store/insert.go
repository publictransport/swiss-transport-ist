package store

import (
	"fmt"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"log"
	"octo.ch/swiss-transport-ist/ist"
	"octo.ch/swiss-transport-ist/schedule"
	"sync/atomic"
)

type Connector struct {
	DB *pg.DB
	stopMeasureCounter uint64
}

func NewConnector() Connector{
	return Connector{}
}

func (c *Connector) InsertStations(entries []*schedule.Stop) (int, error) {
	err := c.DB.Insert(&entries)
	if err != nil {
		return -1, err
	}
	return len(entries), nil
}

// AttributeStopMeasureIds set the id field on StopMeasure
// Why not letting the ORM play by itself?
// because stop measure have a reference to the "next" stop, which is not yet inserted, ths does not yat have an id
func (c *Connector) AttributeStopMeasureIds(sms []*ist.StopMeasure) {
	log.Println(fmt.Sprintf("attributing stop measures ids, starting with %v", c.stopMeasureCounter))
	for _, sm:= range(sms){
		atomic.AddUint64(&c.stopMeasureCounter, 1)
		sm.Id=c.stopMeasureCounter
	}
	for _, sm:= range(sms){
		if sm.NextStopMeasure == nil{
			continue
		}
		sm.NextStopMeasureId = sm.NextStopMeasure.Id
	}
}

func (c *Connector) InsertStopMeasures(entries []*ist.StopMeasure) (int, error) {
	err := c.DB.Insert(&entries)
	if err != nil {
		return -1, err
	}
	return len(entries), nil
}

func (c *Connector) Open() error {
	c.DB = pg.Connect(&pg.Options{
		User:     "postgres",
		Password: "postgres",
		Addr:     "localhost:5432",
		Database: "realtime_dumps",
	})
	err := c.createTables()
	if err != nil {
		return err
	}
	err = c.createIndexes()
	if err != nil {
		return err
	}
	return nil

}

func (c *Connector) createIndexes() error {
	_, err := c.DB.Exec("CREATE INDEX IF NOT EXISTS  operating_day_idx ON stop_measures (operating_day);")
	return err
}
//createTables based on ist.StopMeasure (and others)
func (c *Connector) createTables() error {
	for _, model := range []interface{}{&schedule.Stop{}, &ist.StopMeasure{}} {
		err := c.DB.CreateTable(model, &orm.CreateTableOptions{
			Temp:        false,
			IfNotExists: true,
			FKConstraints: true,
		})
		if err != nil {
			return err
		}
	}
	return nil
}
